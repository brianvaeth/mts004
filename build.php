<?php

$dots = array('.', '..');
$dir =  $argv[1] . '/sass/includes';
$sass = '';
$files = scandir($dir);
foreach($files as $file) {
	if (in_array($file, $dots)) continue;
	$sass .= '@import "includes/' . preg_replace('/.scss/', '', preg_replace('/_/', '', $file)) . '";';
	$sass .= "\n\r";
}
echo $sass;
?>