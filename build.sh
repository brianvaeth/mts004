#!/bin/bash
#


imports=`php -f $1/build.php $1`
rm -f $1/sass/_includes.scss
touch $1/sass/_includes.scss

echo $imports > $1/sass/_includes.scss

sass -t compressed --no-cache $1/sass/styles.scss $1/site/css/styles.css

#rm -f $1/sass/_includes.scss

curl -o $1/site/news/news_rss.xml http://localhost/sublime/MTS004/php/newsxml.php
curl -o $1/site/impact-stories/stories_rss.xml http://localhost/sublime/MTS004/php/storiesxml.php
curl -o $1/site/videos/videos_rss.xml http://localhost/sublime/MTS004/php/videosxml.php

curl -o $1/site/index.php http://localhost/sublime/MTS004/php/index.php
curl -o $1/site/search-results.html http://localhost/sublime/MTS004/php/search-results.php
curl -o $1/site/areas-to-support.html http://localhost/sublime/MTS004/php/areas-to-support.php
curl -o $1/site/how-to-help.html http://localhost/sublime/MTS004/php/how-to-help.php
curl -o $1/site/contact.html http://localhost/sublime/MTS004/php/contact.php
curl -o $1/site/your-gifts-impact.php http://localhost/sublime/MTS004/php/your-gifts-impact.php
curl -o $1/site/news.php http://localhost/sublime/MTS004/php/news.php

for file in $1/php/news/*.php; do 
	echo "$file"; 
	data_re=".+\/(.+).php"
	if [[ $file =~ $data_re ]]; then
		f=${BASH_REMATCH[1]}
		curl -o $1/site/news/$f.html http://localhost/sublime/MTS004/php/news/$f.php
	fi
done

for file in $1/php/impact-stories/*.php; do 
	echo "$file"; 
	data_re=".+\/(.+).php"
	if [[ $file =~ $data_re ]]; then
		f=${BASH_REMATCH[1]}
		curl -o $1/site/impact-stories/$f.html http://localhost/sublime/MTS004/php/impact-stories/$f.php
	fi
done