		</main>

		<footer class="blue-bg">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<a href="http://www.msuaf.org/s/1584/start.aspx" title="Alumni Foundation"><img src="<?php echo $path; ?>img/foundation-logo.png" alt="" class="img-responsive"/></a>
					</div>
					<div class="col-sm-3">
						<div class="h5">contact us</div>
						<p class="sentinal-book-it">1501 South 11th Avenue<br/>Bozeman, MT 59715<br/>800-457-1696</p>
					</div>
					<div class="col-sm-3">
						<div class="h5">stay connected</div>
						<p class="social-links">
							<a href="http://www.facebook.com/mtstatefoundation" title="Facebook"><span class="sr-only">Facebook</span><span class="icon-facebook"></span></a>
							<a href="http://twitter.com/MTStateFound" title="Twitter"><span class="sr-only">Twitter</span><span class="icon-twitter"></span></a>
							<a href="http://www.vimeo.com" title="Vimeo"><span class="sr-only">Vimeo</span><span class="icon-vimeo"></span></a>
						</p>
					</div>
					<div class="col-sm-3">
						<a class="btn" title="MSU ALUMNI FOUNDATION HOME" href="http://www.msuaf.org/s/1584/start.aspx">MSU ALUMNI<br/>FOUNDATION HOME</a>
					</div>
				</div>
			</div>
		</footer>

		<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header"> <!-- BEN -->
						<a id="videoClose" data-dismiss="modal" aria-hidden="true" title="close">
							<span class="icon-cancel"></span>
							<span class="sr-only">Close</span>
						</a>
					</div> <!-- BEN -->
					<div class="modal-body">
						<div class="wrap16x9">
							<iframe width="640" height="480" src="#" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>