<?php

$active = 'how';

include 'header.php' ?>

<?php $jumboclasses = array('how'); include 'jumbotron1.php'; ?>

<section>
	<div class="container-fluid">
		<div class="col-sm-10 col-sm-offset-1">
			<h2 class="text-center">Ways to Give</h2>
			<p class="text-center">Designate a gift or pledge to an area of personal interest or give to the university&rsquo;s area of greatest need. By giving, you help create new opportunities for our students, enrich our faculty and staff, strengthen our academic programs and enhance our campus community.</p>
			<hr/>
			<h3>giving online</h3>
			<p>Giving online is a simple, impactful way to help build a stronger Montana and a better world. Make a donation <a href="#" title="give online">online</a> using the MSU Alumni Foundation&rsquo;s secure giving form.  To give to an area not listed on the MSU Alumni Foundation&rsquo;s giving form, select &ldquo;Other&rdquo; and specify your preferred area in the second step.</p>
			<p>For more information about any type of gift to MSU, please view our Areas to Support. For more detailed information on gift giving, visit the <a href="http://www.msuaf.org/s/1584/start.aspx" title="MSU Foundation">MSU Alumni Foundation</a> page.</p>
			<p><a class="btn" href="#" title="Give online now">Give online now</a></p>

			<h3>Send us your gift</h3>
			<p>To continue to thrive, it takes all of us. By giving, you help strengthen our academic programs and enhance our community. Gifts in the form of checks can be made payable to the &ldquo;Montana State University Foundation&rdquo; with a note in the memo section specifying your intended area to support.</p> 
			<p><strong>Please mail your check to:</strong><br/>MSU Alumni Foundation <br/>P.O. Box 172750<br/> Bozeman, MT<br/>59717-2750</p>

			<h3>Make a planned gift</h3>
			<p>Making a planned gift helps ensure a promising future for Montana State University, and there are countless ways to create a lasting impact.</p>
			<h5>Bequests</h5>  
			<p>The most common form of deferred giving is a bequest contained in a person&rsquo;s will or revocable (&ldquo;living&rdquo;) trust. Bequests are a wonderful way to have an impact beyond your lifetime and to leave a meaningful legacy that makes a difference in the lives of our students and faculty for many years to come.</p>
			<h5>LIFE INCOME GIFTS</h5>  
			<p>Life income gifts benefit both the University and the donor. Donors may receive numerous tax and financial benefits by creating a life income gift, such as a charitable gift annuity or a charitable remainder trust. The donor makes an irrevocable contribution of assets to fund the trust or annuity, gets an immediate income tax deduction for part of the contribution&rsquo;s value, and receives income for life or for a term of between 1 and 20 years. When the trust or annuity term ends, the remaining assets can be directed to support the foundation.</p>
			<h5>CHARITABLE LEAD TRUSTS</h5>
			<p>A charitable lead trust can make an agreed payment to the University for a specific term of years or for someone&rsquo;s life. Thereafter, the lead trust assets are either (a) returned to the person who created the lead trust; this person also receives an income tax deduction when the trust is created or (b) passed on to children, grandchildren or other loved ones and applicable estate or gift taxes on the value of the gift are reduced or completely eliminated.</p>
			<h5>Memorial or Tribute Gifts</h5>  
			<p>Any gift may be designated in memory or honor of a family member, friend or other person. Be sure to indicate your intention in the memo section of your check—or by enclosing a letter stating your intentions—with your gift. There is also a place on the online giving form to make a gift in honor of or in memory of someone.</p>
			<p>The items above are just a few examples of the gift planning options that are available to you. If you would like to know more about how to give a planned gift, please contact the MSU Foundation at 1-800-457-1696 or visit their planned giving website.</p>
			<p><a href="#" title="Learn more about planned giving" class="btn">Learn more about planned giving</a></p>
		</div>
	</div>
</section>

<section>
	<div class="container-fluid">
		<?php $classes = 'top-ruled'; include 'campaign-progress.php'; ?>
	</div>
</section>



<?php include 'footer.php'; ?>