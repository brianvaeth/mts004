<?php

if (in_array('video', $classes)) 
	$url = 'https://player.vimeo.com/video/116267743?portrait=0&byline=0&title=0';
elseif (in_array('news', $classes))
	$url = $path.'news/jabs-hall.html';
else
	$url = $path.'impact-stories/dani-morrison.html';


?>
<a href="<?php echo $url;?>" title="lorem ipsum" class="excerpt <?php echo implode(' ', $classes); ?>">
	<div class="thumb">
		<div class="wrap16x9">
	<?php if (in_array('featured', $classes)): ?>
		<img src="http://lorempixel.com/540/304/people/4" alt=""/> <!-- *** LARGE THUMBNAIL *** -->
	<?php else: ?>
		<img src="http://lorempixel.com/350/197/people/4" alt=""/> <!-- SMALL THUMBNAIL -->
	<?php endif; ?>
	</div>
	</div>
	<div class="content">
		<div class="date">June 22, 2015</div>
		<div class="area">people</div>
		<div class="headline"><?php if (in_array('video', $classes)) 
				echo 'It takes Dani Morrison'; 
			elseif (in_array('news', $classes))
				echo 'Hundreds gather at MSU to celebrate opening of Jab Hall';
			else 
				echo 'Lorem Ipsum'; 
		?></div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		<div class="cta"><?php if (in_array('video', $classes)) 
				echo 'play video'; 
			elseif (in_array('news', $classes))
				echo 'Read full article';
			else 
				echo 'Read her story';
		?></div>
	</div>
</a>