<!doctype html>
<html lang="en-us">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php if (isset($title)) echo $title; else echo 'The Campaign for Montana State University';?></title>

		<script src="<?php echo $path; ?>js/modernizr.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="author" content="Mills College" />
		<meta name="robots" content="all" />
	    	<meta name="apple-mobile-web-app-title" content="The Campaign for Montana State University" />
	    	<link rel="shortcut icon" href="http://www.montana.edu/favicon.ico"/>
		
		<link rel="stylesheet" href="<?php echo $path; ?>css/styles.css" />

		<!-- page-specific stylesheets -->
	    	<?php if (isset($styles)) foreach ($styles as $style) echo $style . "\n"; ?> 

	    	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
		<script src="<?php echo $path;?>js/interactions.js"></script>

	    	<!-- page-specific javascripts -->
	    	<?php if (isset($scripts)) foreach ($scripts as $script) echo $script . "\n"; ?> 


		<!-- GOOGLE CUSTOM SEARCH -->
	    	<script type="text/javascript">
	    		
	    		window.__gcse = {
				parsetags: 'explicit',
				callback: function(){
					if (document.readyState == 'complete') {
						google.search.cse.element.go('searchbox');
						google.search.cse.element.go('search-results');
						$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
					}
					else {
						google.setOnLoadCallback(function(){
							google.search.cse.element.go('searchbox');
							google.search.cse.element.go('search-results');
							$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
						});
					}
				}
			};

			(function($) {
				var cx = '014965969058198357657:q5oh1uiq1ui';//'009057559413264856654:ykk_ospy3gc'; //; '003587693938171614370:inikzu6wtdw'
				var gcse = document.createElement('script'); 
				gcse.type = 'text/javascript'; 
				gcse.async = true;
				gcse.src = (document.location.protocol == 'https' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
				var s = document.getElementsByTagName('script')[0]; 
				s.parentNode.insertBefore(gcse, s);
			})(jQuery);
	        </script>
	</head>
	<body class="<?php echo $active.' '.$classes;?>">
		<header>
			<div id="top-bar" class="blue-bg">
				<div class="container-fluid">
					<div>
						<a href="http://www.montana.edu/" title="MONTANA STATE"><img src="<?php echo $path; ?>img/mts-logo.svg" alt=""/></a>
						
						<div class="pull-right">
							<a href="#searchbox"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
								<span class="glyphicon-search glyphicon"></span>
								<span class="icon-cancel"></span>
								<span class="sr-only">menu</span>
							</a> 
						</div>
					</div>
					<div id="searchbox" class="collapse">
						<div><div class="gcse-searchbox-only" data-resultsUrl="<?php echo $path; ?>search-results.html"></div></div>
					</div>
				</div>
			</div>
			<div id="logo-row">
				<div class="container-fluid">
					<div><a href="<?php echo $path; ?>index.php" title="campaign home"><img src="<?php echo $path; ?>img/main-logo.svg" alt=""/></a></div>
					<div class="pull-right">
						<a href="#main-nav"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
							<span class="">menu</span>
							<span class="icon-menu"></span>
							<span class="icon-cancel"></span>
						</a> 
					</div>
				</div>
			</div>
			
			<div class="container-fluid">
				<nav class="navbar navbar-default">
					<div class="collapse navbar-collapse" id="main-nav">
						<ul class="nav navbar-nav">
							<li<?php if(isset($active) && $active=='areas') echo ' class="active"'; ?>><a href="<?php echo $path; ?>areas-to-support.html" title="Areas to Support">Areas to Support</a></li>
							<li<?php if(isset($active) && $active=='how') echo ' class="active"'; ?>><a href="<?php echo $path; ?>how-to-help.html" title="How to Help">How to Help</a></li>
							<li<?php if(isset($active) && $active=='impact') echo ' class="active"'; ?>><a href="<?php echo $path; ?>your-gifts-impact.php" title="Your Gift's Impact">Your Gift&rsquo;s Impact</a></li>
							<li<?php if(isset($active) && $active=='news') echo ' class="active"'; ?>><a href="<?php echo $path; ?>news.php" title="News">News</a></li>
							<li<?php if(isset($active) && $active=='contact') echo ' class="active"'; ?>><a href="<?php echo $path; ?>contact.html" title="Contact">Contact</a></li>
							<li><a href="#" title="Give Now" class="btn">Give Now</a></li>
						</ul>						
					</div>
				</nav>
			</div>



		</header> 
		<main>