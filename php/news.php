<?php
$active = 'news';
$path = '';
$classes = 'archive';

include $path.'header.php' ; ?>

<section class="simple-intro">
	<h1 class="text-center">Campaign News</h1>
	<div class="container-fluid pagination-container" id="news">
		<div class="pagination-canvas">
			<?php


	$php_stuff = <<<HEREDOC
<?php

			// LOAD THE NEWS.XML DATA INTO AN ARRAY //
			\$items = array();
			\$xml = new SimpleXMLElement(file_get_contents('news/news_rss.xml'));

			foreach(\$xml->channel[0]->item as \$child):
				\$items[] = \$child;
			endforeach;

			// ROWS OF 3 EACH //
			\$rows = array_chunk(\$items, 3); 

			// PAGES ARE 2 ROWS EACH //
			\$pages = array_chunk(\$rows, 2);

			// WHAT PAGE IS REQUESTED? //
			\$page = (isset(\$_GET['page']) && \$_GET['page']) ? intval(\$_GET['page']) : 1; 

			// TIMEZONE //
			date_default_timezone_set('America/Los_Angeles');

			// ITERATE AND ECHO THE PAGE OF ITEMS //
			foreach(\$pages[\$page-1] as \$row): ?>
				<div class="row">
					<?php foreach(\$row as \$item): 
						\$ns = \$item->getNamespaces(true);
						\$media = \$item->children(\$ns['media']); ?>
						<div class="col-sm-4">
							<a href="<?php echo \$item->link;?>" title="<?php echo \$item->title;?>" class="excerpt news">
								<div class="thumb">
									<div class="wrap16x9">
										<img src="<?php echo \$media->content->thumbnail->attributes()->url;?>" alt=""> <!-- SMALL THUMBNAIL -->
									</div>
								</div>
								<div class="content">
									<div class="date"><?php echo date('F d, Y', strtotime(\$item->pubDate));?></div>
									<div class="area">people</div>
									<div class="headline"><?php echo \$item->description;?></div>
									<div class="cta">Read full article</div>
								</div>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endforeach; ?>
HEREDOC;

echo $php_stuff; ?>


			<!-- PAGINATION -->
			<nav>
				<ul class="pagination">
<?php $php_stuff = <<<HEREDOC
					<li <?php if(\$page == 1) echo 'class="disabled"';?>>
						<a href="?page=<?php echo \$page - 1;?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
					</li>
					
					<?php for(\$i = 0; \$i < count(\$pages); \$i++): ?>
						<li<?php if(\$i + 1 == \$page) echo ' class="active"';?>>
							<a href="?page=<?php echo \$i + 1;?>"><?php echo \$i + 1;?></a>
						</li>
					<?php endfor; ?>
					
					<li <?php if(\$page >= count(\$pages)) echo 'class="disabled"';?>>
						<a href="?page=<?php echo \$page + 1;?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
					</li>
HEREDOC;
echo $php_stuff;
?>			
				</ul>
			</nav>
		</div>
	</div>
</section>

<section class="campaing-progress-section">
	<div class="container-fluid top-ruled">
		<?php include $path.'campaign-progress.php'; ?>
	</div>
</section>

<?php include $path.'footer.php'; ?>