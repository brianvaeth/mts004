					<div class="row">
						<div class="col-sm-6">
							<h3>An investment in <?php echo isset($area) ? $area : 'people'; ?></h3>
							<p>As a land-grant institution, Montana State is both in the community and of the community. We&rsquo;re committed to outreach, engagement and service learning, and we encourage students and faculty to work closely with communities as we extend our classrooms and laboratories across Montana and the world. Your support will help students and faculty succeed as they apply their skills and knowledge in real-world settings, equipping our graduates with multicultural worldviews and an ethic of lifelong service.</p>
						</div>
						<div class="col-sm-6">
							<?php $classes = array('video', 'featured'); include('excerpts/excerpt.php'); ?>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<h4>Student Scholarships and Fellowships</h4>
							<p>We believe acceptance to MSU should be based on talent, not on family finances. Privately funded scholarships and fellowships help a diverse range of students contribute to our community of discovery and earn the education they need to make an impact.</p>
						</div>
						<div class="col-sm-3">
							<h5>Current initiatives</h5>
							<ul>
								<li><a href="#" title="Montana Resident Premier Scholarship">Montana Resident Premier Scholarship</a></li>
								<li><a href="#" title="Example Fund">Example Fund</a></li>
								<li><a href="#" title="Montna Example Fund">Montana Example Fund</a></li>
								<li><a href="#" title="Example Fund">The Fund of Grants</a></li>
							</ul>
						</div>
						<div class="col-sm-3">
							<?php $classes = array('video', 'short'); include('excerpts/excerpt.php'); ?>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<h4>Investing in Professorships and Chairs</h4>
							<p>We believe acceptance to MSU should be based on talent, not on family finances. Privately funded scholarships and fellowships help a diverse range of students contribute to our community of discovery and earn the education they need to make an impact.</p>
						</div>
						<div class="col-sm-3">
							<h5>Current initiatives</h5>
							<ul>
								<li><a href="#" title="Montana Resident Premier Scholarship">Montana Resident Premier Scholarship</a></li>
								<li><a href="#" title="Example Fund">Example Fund</a></li>
								<li><a href="#" title="Montna Example Fund">Montana Example Fund</a></li>
								<li><a href="#" title="Example Fund">The Fund of Grants</a></li>
							</ul>
						</div>
						<div class="col-sm-3">
							<?php $classes = array('video', 'short'); include('excerpts/excerpt.php'); ?>
						</div>
					</div>


					<div class="row">
						<div class="col-sm-6">
							<h4>Investing in Student Success Funds</h4>
							<p>We believe acceptance to MSU should be based on talent, not on family finances. Privately funded scholarships and fellowships help a diverse range of students contribute to our community of discovery and earn the education they need to make an impact.</p>
						</div>
						<div class="col-sm-3">
							<h5>Current initiatives</h5>
							<ul>
								<li><a href="pdf/Fraternity-Sorority-20150730-web.pdf" title="Fraternity and Sorority Leadership" target="_blank">Fraternity and Sorority Leadership</a></li>
								<li><a href="#" title="Example Fund">Example Fund</a></li>
								<li><a href="#" title="Montna Example Fund">Montana Example Fund</a></li>
								<li><a href="#" title="Example Fund">The Fund of Grants</a></li>
							</ul>
						</div>
						<div class="col-sm-3">
							<?php $classes = array('video', 'short'); include('excerpts/excerpt.php'); ?>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<h4>Investing in Faculty-led Research and Leadership Grants</h4>
							<p>We believe acceptance to MSU should be based on talent, not on family finances. Privately funded scholarships and fellowships help a diverse range of students contribute to our community of discovery and earn the education they need to make an impact.</p>
						</div>
						<div class="col-sm-3">
							<h5>Current initiatives</h5>
							<ul>
								<li><a href="#" title="Montana Resident Premier Scholarship">Montana Resident Premier Scholarship</a></li>
								<li><a href="#" title="Example Fund">Example Fund</a></li>
								<li><a href="#" title="Montna Example Fund">Montana Example Fund</a></li>
								<li><a href="#" title="Example Fund">The Fund of Grants</a></li>
							</ul>
						</div>
						<div class="col-sm-3">
							<?php $classes = array('video', 'short'); include('excerpts/excerpt.php'); ?>
						</div>
					</div>