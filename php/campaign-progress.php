<?php 


?>
<!-- START Fundraiser -->  
<div class="campaign-progress clearfix <?php echo $classes;?>">        
	<div class="h2 text-center">campaign progress</div>
	<div class="left-side">
		<div class="progress-canvas">
			<div class="indicator_curValue"></div>
			<div class="indicator_bar_bg"></div>
			<div class="indicator_counter counter-graphic-left">
				<span class="indicator_counter_val">$181,400,000</span>
				<span class="indicator_counter_val_max">$300,000,000</span>
				<span class="indicator_counter_percent"></span>
			</div>
		</div>
	</div>
	<div class="right-side">
		<div>Our Goal</div>
		<div>$300 Million</div>
	</div>
</div>
<!-- END Fundraiser -->