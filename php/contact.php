<?php

$active = 'contact';
$path = '';
include 'header.php' ?>

<section class="simple-intro">
	<div class="container-fluid text-left constrained">
		<h1 class="">Contact us</h1>
		
		<p class="tight"><strong>Montana State University Alumni Foundation</strong><br/>P.O. Box 172750<br/>Bozeman, MT<br/>59717-2750<br/><a href="mailto:info@msauf.org" title="info@msuaf.org">info@msuaf.org</a></p>

		<div class="h5">stay connected</div>
		<p class="social-links">
			<a href="http://www.facebook.com/mtstatefoundation" title="Facebook"><span class="sr-only">Facebook</span><span class="icon-facebook"></span></a>
			<a href="http://twitter.com/MTStateFound" title="Twitter"><span class="sr-only">Twitter</span><span class="icon-twitter"></span></a>
			<a href="http://www.vimeo.com" title="Vimeo"><span class="sr-only">Vimeo</span><span class="icon-vimeo"></span></a>
		</p>

		<p><a class="btn" title="MSU ALUMNI FOUNDATION HOME" href="http://www.msuaf.org/s/1584/start.aspx">MSU ALUMNI<br/>FOUNDATION HOME</a></p>
	</div>
</section>

<section class="top-ruled">
	<div class="container-fluid">
		<?php include 'campaign-progress.php'; ?>
	</div>
</section>


<?php include 'footer.php'; ?>