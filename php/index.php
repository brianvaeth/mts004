<?php

$active = 'home';

include 'header.php' ?>

<?php $jumboclasses = array('home'); include 'jumbotron1.php'; ?>


<section>
	<div class="container-fluid text-center constrained">
		<div class="h2">It takes all of us.</div>
		<p>We all have a story of how Montana State has impacted our communities and our lives, and your story can inspire others to create a better Montana.</p>
		<a class="btn" href="your-gifts-impact.php" title="share your story">share your story</a>
	</div>
</section>

<section class="top-ruled">
	<div class="container-fluid">
		<?php include 'video-gallery.php';?>
	</div>
</section>

<section class="top-ruled">
	<div class="container-fluid">
		<?php include 'campaign-progress.php'; ?>
	</div>
</section>

<?php $jumboclasses = array('areas'); include 'jumbotron1.php'; ?>

<section>
	<div class="container-fluid">
		<h2 class="text-center">Recent News</h2>
		<h5 class="text-center"><a href="news.php" title="all news">All News</a></h5>
		<hr/>

		<?php


$php_stuff = <<<HEREDOC
<?php

		// LOAD THE NEWS.XML DATA INTO AN ARRAY //
		\$items = array();
		\$xml = new SimpleXMLElement(file_get_contents('news/news_rss.xml'));

		foreach(\$xml->channel[0]->item as \$child):
			\$items[] = \$child;
		endforeach;

		// ROWS OF 3 EACH //
		\$rows = array_chunk(\$items, 3); 

		// TIMEZONE //
		date_default_timezone_set('America/Los_Angeles');

		// ITERATE AND ECHO THE FIRST ROW //  ?>
		<div class="row">
			<?php foreach(\$rows[0] as \$item): 
				\$ns = \$item->getNamespaces(true);
				\$media = \$item->children(\$ns['media']); ?>
				<div class="col-sm-4">
					<a href="<?php echo \$item->link;?>" title="<?php echo \$item->title;?>" class="excerpt news">
						<div class="thumb">
							<div class="wrap16x9">
								<img src="<?php echo \$media->content->thumbnail->attributes()->url;?>" alt=""> <!-- SMALL THUMBNAIL -->
							</div>
						</div>
						<div class="content">
							<div class="date"><?php echo date('F d, Y', strtotime(\$item->pubDate));?></div>
							<div class="area">people</div>
							<div class="headline"><?php echo \$item->description;?></div>
							<div class="cta">Read full article</div>
						</div>
					</a>
				</div>
			<?php endforeach; ?>
		</div>

HEREDOC;

echo $php_stuff; ?>

	</div>
</section>

<?php include 'footer.php'; ?>