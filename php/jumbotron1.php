
<section class="jumbo <?php echo implode(' ', $jumboclasses);?>">
	<div>
		<div>
			<div class="content-box">
				
				<?php if (in_array('how', $jumboclasses)): ?>
					<div class="h1">How to help</div>
					<p>There are many ways to support MSU and to help build a stronger Montana. Your gift ensures MSU's future success and builds on our commitment to advanced thinking, shared knowledge, and groundbreaking research. Learn more about the Areas to Support and the various ways you can do what&rsquo;s right for the sons and daughters of Montana, and beyond.</p>
					<a href="#" class="btn" title="Make a gift now">Make a gift now</a>
				<?php elseif (in_array('home', $jumboclasses)): ?>
					<div class="h1">To create smarter, healthier communities.</div>
					<p>It takes Montana State University. And it takes you. <a href="areas-to-support.html" title="Learn more about the campaign">Learn more about the campaign</a></p>
				<?php elseif (in_array('areas', $jumboclasses)): ?>
					<div class="h1">Make a lasting impact.</div>
					<p>Designate a gift or pledge to an area of personal interest or give to the university&rsquo;s area of greatest need. By giving, you help create new opportunities for our students, enrich our faculty and staff, strengthen our academic programs and enhance our campus community.</p>
					<a href="areas-to-support.html" class="btn" title="Areas to support">Areas to support</a>
				<?php endif; ?>

			</div>
		</div>
	</div>

<?php if (in_array('home', $jumboclasses)): ?>

	<div>
		<div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6">
						<div class="call-to-action">
									<div><img src="img/home-hero-areas.jpg" alt=""/></div>
									<div class="headline">Imagine a better<br/>Montana.</div>
									<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
									<div><a href="how-to-help.php" title="Learn how you can help">Learn how you can help</a></div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="call-to-action">
									<div><img src="img/home-hero-how.jpg" alt=""/></div>
									<div class="headline">We&rsquo;re making a<br/>difference.</div>
									<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
									<div><a href="your-gifts-impact.php" title="Learn how you can help">See how your support helps</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php endif; ?>
		
</section>