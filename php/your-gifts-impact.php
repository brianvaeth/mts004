<?php

$active = 'impact';
$path = '';
$classes = 'archive';

include $path . 'header.php' ?>

<section class="simple-intro">
	<div class="container-fluid">
		<h1>Your gift&rsquo;s impact</h1>
	</div>
</section>

<section>
	<div class="container-fluid top-ruled">
		<h2 class="text-center">watch what it takes</h2>

		<div class="pagination-container" id="videos">
			<div class="pagination-canvas">
				<?php

$php_stuff = <<<HEREDOC
	<?php

				// LOAD THE NEWS.XML DATA INTO AN ARRAY //
				\$items = array();
				\$xml = new SimpleXMLElement(file_get_contents('videos/videos_rss.xml'));
				foreach(\$xml->channel[0]->item as \$child):
					\$items[] = \$child;
				endforeach;

				// ROWS OF 3 EACH //
				\$rows = array_chunk(\$items, 3); 

				// PAGES ARE 2 ROWS EACH //
				\$pages = array_chunk(\$rows, 2);

				// WHAT PAGE IS REQUESTED? //
				\$page = (isset(\$_GET['p']) && \$_GET['p']) ? intval(\$_GET['p']) : 1; 

				// ITERATE AND ECHO THE PAGE OF ITEMS //
				foreach(\$pages[\$page-1] as \$row): ?>
					<div class="row">
						<?php foreach(\$row as \$item): 
							\$ns = \$item->getNamespaces(true);
							\$media = \$item->children(\$ns['media']);
							\$ouc = \$item->children(\$ns['ouc']);?>
							<div class="col-sm-4">
								<a href="<?php echo \$ouc->videourl;?>" title="<?php echo \$item->title;?>" class="excerpt video">
									<div class="thumb">
										<div class="wrap16x9">
											<img src="<?php echo \$media->content->thumbnail->attributes()->url;?>" alt=""> <!-- SMALL THUMBNAIL -->
										</div>
									</div>
									<div class="content">
										<div class="headline"><?php echo \$item->title;?></div>
										<p><?php echo \$item->description;?></p>
										<div class="cta">Play video</div>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endforeach; ?>
HEREDOC;

echo $php_stuff; ?>


				<!-- PAGINATION -->
				<nav>
					<ul class="pagination">
<?php $php_stuff = <<<HEREDOC
						<li <?php if(\$page == 1) echo 'class="disabled"';?>>
							<a href="?p=<?php echo \$page - 1;?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
						</li>
						
						<?php for(\$i = 0; \$i < count(\$pages); \$i++): ?>
							<li<?php if(\$i + 1 == \$page) echo ' class="active"';?>>
								<a href="?p=<?php echo \$i + 1;?>"><?php echo \$i + 1;?></a>
							</li>
						<?php endfor; ?>
						
						<li <?php if(\$page >= count(\$pages)) echo 'class="disabled"';?>>
							<a href="?p=<?php echo \$page + 1;?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
						</li>
HEREDOC;
echo $php_stuff;
?>			
					</ul>
				</nav>
			</div>
		</div>		

	</div>
</section>

<section>
	<div class="container-fluid top-ruled">
		<h2 class="text-center">read what it takes</h2>

		<div class="pagination-container" id="stories">
			<div class="pagination-canvas">
				<?php

$php_stuff = <<<HEREDOC
	<?php

				// LOAD THE NEWS.XML DATA INTO AN ARRAY //
				\$items = array();
				\$xml = new SimpleXMLElement(file_get_contents('impact-stories/stories_rss.xml'));
				foreach(\$xml->channel[0]->item as \$child):
					\$items[] = \$child;
				endforeach;

				// ROWS OF 4 EACH //
				\$rows = array_chunk(\$items, 4); 

				// PAGES ARE 2 ROWS EACH //
				\$pages = array_chunk(\$rows, 2);

				// WHAT PAGE IS REQUESTED? //
				\$page = (isset(\$_GET['page']) && \$_GET['page']) ? intval(\$_GET['page']) : 1; 

				// ITERATE AND ECHO THE PAGE OF ITEMS //
				foreach(\$pages[\$page-1] as \$row): ?>
					<div class="row">
						<?php foreach(\$row as \$item): 
							\$ns = \$item->getNamespaces(true);
							\$media = \$item->children(\$ns['media']);
							\$ouc = \$item->children(\$ns['ouc']);?>
							<div class="col-sm-3">
								<a href="<?php echo \$item->link;?>" title="<?php echo \$item->title;?>" class="excerpt">
									<div class="thumb">
										<div class="wrap16x9">
											<img src="<?php echo \$media->content->thumbnail->attributes()->url;?>" alt=""> <!-- SMALL THUMBNAIL -->
										</div>
									</div>
									<div class="content">
										<div class="headline"><?php echo \$item->title;?></div>
										<p><?php echo \$item->description;?></p>
										<div class="cta"><?php echo \$ouc->label;?></div>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endforeach; ?>
HEREDOC;

echo $php_stuff; ?>


				<!-- PAGINATION -->
				<nav>
					<ul class="pagination">
<?php $php_stuff = <<<HEREDOC
						<li <?php if(\$page == 1) echo 'class="disabled"';?>>
							<a href="?page=<?php echo \$page - 1;?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
						</li>
						
						<?php for(\$i = 0; \$i < count(\$pages); \$i++): ?>
							<li<?php if(\$i + 1 == \$page) echo ' class="active"';?>>
								<a href="?page=<?php echo \$i + 1;?>"><?php echo \$i + 1;?></a>
							</li>
						<?php endfor; ?>
						
						<li <?php if(\$page >= count(\$pages)) echo 'class="disabled"';?>>
							<a href="?page=<?php echo \$page + 1;?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
						</li>
HEREDOC;
echo $php_stuff;
?>			
					</ul>
				</nav>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container-fluid top-ruled">
		<h2 class="text-center">share your story</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<p class="text-center">We all have a story of how Montana State has impacted our communities and our lives, and your story can inspire others to create a better Montana.</p>
			</div>
		</div>
			
		<div id="form-holder"><?php echo file_get_contents('form-handler.php'); ?></div>
	</div>

	<script>
	(function($){
		$(document).ready(function(){
			var canvas = $('#form-holder');
			var form = canvas.find('form');

			form.on('submit', function(e){
				e.preventDefault();
				canvas.css('height', canvas.height()).animate({opacity:0}, 'fast', function(){
					$.post(window.location.href, form.serialize(), function(data){
						canvas.empty().append($(data).find('#form-holder'));
						canvas.animate({opacity:1}, 'fast');
					});
				});
			});
		});
	})(jQuery);
	</script>
</section>



<section>
	<div class="container-fluid top-ruled">
		<?php $classes = array(); include $path . 'campaign-progress.php'; ?>
	</div>
</section>


<?php include $path . 'footer.php'; ?>