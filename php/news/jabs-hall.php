<?php

$active = 'news';
$path = '../';
$classes = 'single';

include $path.'header.php' ?>

<article>
	<section id="breadcrumb">
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li><a href="<?php echo $path;?>index.php" title="home">Home</a></li>
				<li><a href="<?php echo $path;?>news.php" title="news">News</a></li>
				<li class="active">Jabs Hall</li>
			</ol>
		</div>
	</section>

	<section class="sharethis">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
					<span class="st_facebook_large" displayText="Facebook"></span>
					<span class="st_twitter_large" displayText="Tweet"></span>
					<?php /* <span class="st_googleplus_large" displayText="Google +"></span> */ ?>
				</div>
			</div>
			<script src="http://w.sharethis.com/button/buttons.js"></script>
			<script>stLight.options({publisher: "88951ca8-4c02-405a-8bb0-a9d950c0c635", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
		</div>
	</section>

	<section class="headline">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
					<div class="dateline">June 2, 2015</div>
					<h1>Jabs Hall: Open for business</h1>
					<h2>Hundreds gather at MSU to celebrate the opening of Jabs Hall</h2>
				</div>
			</div>
		</div>
	</section>

	<section class="featured-image">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
					<figure>
						<div class="wrap16x9"><img src="http://lorempixel.com/710/306/business/1" alt=""/></div>
						<figcaption>Lorem ipsum dolor sit amet, justo pellentesque a lectus sit, aliquam ut eget, et id. Lorem ipsum dolor sit amet, justo pellentesque a lectus sit, aliquam ut eget, et id.</figcaption>
					</figure>
				</div>
			</div>
		</div>
	</section>

	<section class="article-content">
		<div class="container-fluid constrained">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc malesuada arcu vel urna varius facilisis. In malesuada, sapien fermentum aliquam tempus, massa nulla efficitur libero, vitae maximus lorem neque in elit. Phasellus hendrerit sapien sit amet turpis convallis, sagittis placerat ligula posuere. Phasellus elementum at justo et congue. Duis id varius velit, sed mattis mi. Nulla facilisi. Ut eget ultricies tortor. In hac habitasse platea dictumst. Quisque non ultrices magna. Nulla facilisi. Praesent id risus semper enim laoreet sollicitudin. Duis viverra tincidunt nunc, consectetur laoreet orci venenatis eget.</p>
			<p>Proin aliquam, <a href="#" title="i am an inline link">augue vestibulum iaculis condimentum</a>, nunc magna lacinia tortor, maximus lobortis sem quam eget urna. Aliquam pharetra consectetur finibus. Cras sit amet condimentum tellus. Aenean pulvinar finibus orci vel dignissim. Donec suscipit efficitur lectus, quis vulputate dolor interdum et. Sed quis ante consectetur, molestie odio sed, consequat sapien. Sed nec nibh laoreet, lacinia diam et, pulvinar dolor. Nulla facilisi. Etiam cursus ex nisl, ac fermentum augue efficitur non. Pellentesque facilisis augue a est molestie fermentum. Pellentesque ac sodales ex. Vestibulum malesuada lorem nec urna fringilla, vitae eleifend mi eleifend. Morbi commodo velit tellus, finibus euismod augue convallis at. Nulla ut posuere leo. Aliquam vitae consectetur odio, ac dictum arcu.</p>
			
			<h3>Headline style in article content (h3)</h3>	
			<p>Quisque a magna eget arcu viverra vulputate. Praesent a pulvinar lacus. Nam vel mattis purus. Donec vehicula, arcu congue tempus tempor</p>
			<figure><img src="http://lorempixel.com/400/300/business/2" alt=""/><figcaption>This is a 400x300 FIGURE with a FIGCAPTION. This will need to be a snippet.</figcaption></figure>
			<p>risus enim pellentesque lectus, eget feugiat eros nunc ut nisi. Praesent finibus vitae ligula ut pellentesque. Etiam hendrerit interdum feugiat. Donec fermentum purus vel tellus convallis, sit amet condimentum elit pretium. Aenean eu lorem at dui pulvinar vestibulum. Sed sit amet nibh odio. Etiam iaculis dictum purus. Pellentesque lacinia, eros eu luctus dictum, orci lectus semper odio, sit amet fermentum purus tellus sed risus. Ut nec felis maximus, feugiat leo et, mollis dolor. Nullam dapibus hendrerit ante, vitae fringilla tellus tempus id. Aenean tempor augue at tellus condimentum, id fermentum mi posuere. Donec efficitur massa elementum elementum blandit.</p>
			<p><strong>This is a strong tag</strong><br/><a href="#" class="btn" title="a button">I am a button</a></p>

			<h4>This is another headline style (h4)</h4>
			<ul>
				<li>I am a list item</li>
				<li>This is also a list item</li>
				<li><a href="#" title="link">Here is a link in a list</a></li>
				<li>I am a bit longer. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc malesuada arcu vel urna varius facilisis.</li>
			</ul>
	
			<p>Donec luctus gravida nisi. Ut urna ipsum, accumsan a dapibus sit amet, tempor ut lectus. Integer aliquam malesuada enim, venenatis dapibus neque mattis ac. Phasellus nec ex in diam egestas pellentesque. Proin sapien nisl, lacinia at mattis id, auctor at ligula. Maecenas quis velit vel tortor dictum feugiat. Nulla cursus purus at nisl accumsan viverra. Nulla facilisi. Mauris volutpat ut dolor eu ultricies. In pulvinar elit nunc, eu pretium lectus fermentum in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed aliquam vel nisi quis finibus. Suspendisse a placerat sem, sed ornare ex. Proin et molestie ipsum. Sed blandit, purus sit amet eleifend fringilla, eros massa volutpat arcu, a tincidunt nunc sem id ipsum. Donec vitae elementum ipsum.</p>
			<p>Donec eu dictum sem, at congue nulla. Pellentesque id est dapibus, iaculis ex nec, maximus velit. Nunc rutrum velit nec malesuada eleifend. Suspendisse vehicula risus eu interdum porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ullamcorper mi ac tincidunt tincidunt. Nunc ultricies cursus lectus, quis congue velit rutrum a. Fusce scelerisque mattis ante, eu fermentum magna eleifend nec. Nam sit amet nulla eu turpis blandit eleifend. Integer aliquet diam eget neque vestibulum mattis. Nam in porttitor dui. Maecenas non cursus mi. Duis mollis justo suscipit, convallis velit ac, gravida nisl. Nam ornare sem et augue placerat accumsan. Ut a dictum justo, a gravida purus.</p>
		</div>
	</section>
</article>


<?php include $path.'footer.php'; ?>