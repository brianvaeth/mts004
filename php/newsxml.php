<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n"; ?>
<rss xmlns:media="http://search.yahoo.com/mrss/" xmlns:ouc="http://omniupdate.com/XSL/Variables" version="2.0">
	<channel>
		<title>Montana News Dev Feed</title>
		<link>http://www.uno.edu/z-omniupdate/news-home.aspx</link>
		<description>UNO Development Feed</description>
		<language>en-us</language>
		<lastBuildDate>Mon, 03 Aug 2015 07:22:46 -0700</lastBuildDate>
		<docs>http://blogs.law.harvard.edu/tech/rss</docs>
		<generator>OmniUpdate (OU Publish)</generator>
		<?php for ($i = 0; $i < 13; $i++): 
		
		?><item>
			<title>Jabs Hall: Open for Buisiness</title>
			<link>news/jabs-hall.html</link>
			<description>Hundreds gather at MSU to celebrate opening of Jab Hall</description>
			<author>Jaswanthi Puli &lt;jpuli1@uno.edu&gt;</author>
			<pubDate>Mon, 03 Aug 2015 07:22:46 -0700</pubDate>
			<media:content url="http://lorempixel.com/350/197/people/<?php $nbr = rand(1,10); echo $nbr?>">
				<media:title>Lorem ipsum dolor</media:title>
				<media:description/>
				<media:thumbnail url="http://lorempixel.com/350/197/people/<?php echo $nbr;?>"/>
				<media:keywords />
			</media:content>
			<ouc:category>people</ouc:category>
		</item>
		<?php endfor; ?>
	</channel>
</rss>
