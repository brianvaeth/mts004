<?php

$active = 'areas';

include 'header.php' ?>

<section>


	<div id="areas-to-support-carousel" class="carousel slide" data-ride="carousel" data-interval="0">
		<!-- Indicators -->
		<div class="indicators-wrapper blue-bg">
			<div class="container-fluid">
				<div class="row">
					<ol class="carousel-indicators">
						<li data-target="#areas-to-support-carousel" data-slide-to="0" class="active col-sm-4">
							<div class="wrap16x9"><img src="http://lorempixel.com/350/197/people/4" alt="" /></div>
							<div>
								<h2>Investing in people</h2>
								<p>We have a vision for our future that hinges on a single powerful ambition: to provide our students and faculty members with the resources and opportunities they need to succeed at Montana State and in the world.</p>
								<p><a class="btn" href="#" title="View the people that need support">View the people to support</a></p>
							</div>
						</li>
						<li data-target="#areas-to-support-carousel" data-slide-to="1" class="col-sm-4">
							<div class="wrap16x9"><img src="http://lorempixel.com/350/197/people/6" alt="" /></div>
							<div>
								<h2>Investing in places</h2>
								<p>To continue to thrive, we must have a campus where students and faculty can interact, where facilities are integrated for collaboration, and where every learner is free to pursue research and discovery.</p>
								<p><a class="btn" href="#" title="View the places that need support">View the places to support</a></p>
							</div>
						</li>
						<li data-target="#areas-to-support-carousel" data-slide-to="2" class="col-sm-4">
							<div class="wrap16x9"><img src="http://lorempixel.com/350/197/people/5" alt="" /></div>
							<div>
								<h2>Investing in programs</h2>
								<p>To attract the best students and provide a world-class education, we must deliver enhanced learning opportunities, access to advanced thinking, and a stronger connection to the global community.</p>
								<p><a class="btn" href="#" title="View the programs that need support">View the programs to support</a></p>
							</div>
						</li>
					</ol>
				</div>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#areas-to-support-carousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#areas-to-support-carousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>

		<!-- Wrapper for slides -->
		<div class="container-fluid">
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<?php include 'areas/people.php'; ?>
				</div>

				<div class="item">
					<?php include 'areas/places.php'; ?>
				</div>

				<div class="item">
					<?php include 'areas/programs.php'; ?>
				</div>
			</div>
		</div>
	</div>


</section>

<section>
	<div class="container-fluid">
		<?php include 'campaign-progress.php'; ?>
	</div>
</section>



<?php include 'footer.php'; ?>