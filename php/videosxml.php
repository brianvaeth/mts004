<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n"; ?>
<rss xmlns:media="http://search.yahoo.com/mrss/" xmlns:ouc="http://omniupdate.com/XSL/Variables" version="2.0">
	<channel>
		<title>Montana Videos Dev Feed</title>
		<link>http://www.uno.edu/z-omniupdate/news-home.aspx</link>
		<description>Montana Videos Development Feed</description>
		<language>en-us</language>
		<lastBuildDate>Mon, 03 Aug 2015 07:22:46 -0700</lastBuildDate>
		<docs>http://blogs.law.harvard.edu/tech/rss</docs>
		<generator>OmniUpdate (OU Publish)</generator>
		<?php for ($i = 0; $i < 13; $i++): 
		
		?><item>
			<title>It Takes Dani Morrison</title>
			<link>videos/dani-morrison.html</link>
			<description>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</description>
			<author>Jaswanthi Puli &lt;jpuli1@uno.edu&gt;</author>
			<pubDate>Mon, 03 Aug 2015 07:22:46 -0700</pubDate>
			<media:content url="http://lorempixel.com/350/197/people/<?php $nbr = rand(1,10); echo $nbr?>">
				<media:title>Lorem ipsum dolor</media:title>
				<media:description/>
				<media:thumbnail url="http://lorempixel.com/350/197/people/<?php echo $nbr;?>"/>
				<media:keywords />
			</media:content>
			<ouc:category>people</ouc:category>
			<ouc:videourl>https://player.vimeo.com/video/116267743?portrait=0&amp;byline=0&amp;title=0</ouc:videourl>
		</item>
		<?php endfor; ?>
	</channel>
</rss>