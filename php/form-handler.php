<?php 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$err_name = !$_POST['name'];
	$err_email = !$_POST['email'];

	if (!$err_name && !$err_email) :

		// send an email //
		$headers   = array();
		$headers[] = "MIME-Version: 1.0";
		$headers[] = "Content-type: text/plain; charset=iso-8859-1";
		$headers[] = "Reply-To: {$_POST['name']} <{$_POST['email']}>";
		$headers[] = "From: {$_POST['name']} <{$_POST['email']}>";
		$headers[] = "Subject: Impact Story Submitted";
		$headers[] = "X-Mailer: PHP/".phpversion();

		// The message
		$message = array();
		$message[] = 'FORM VALUES';
		$message[] = '===========================================================';
		$message[] = 'Name: ' . $_POST['name'];
		$message[] = 'Email: ' . $_POST['email'];
		$message[] = 'I AM A/AN: ' . $_POST['i-am-a'];
		$message[] = 'You may contact me: ' . $_POST['may-contact'];
		$message[] = 'I am interested in providing video/audio: ' . $_POST['contact-me'];
		
		$message[] = '=========== MY STORY ======================';
		$message[] = wordwrap($_POST['the-story'], 70, "\r\n");

		$message[] = '==============================';

		if (isset($_POST['i-authorize']))
			$message[] = 'I authorize you to use my story';
		else
			$message[] = 'I DO NOT authorize you to use my story';

		// Send
		$to_email = 'bvaeth@gmail.com'; // *** should be: 'alumni@msuaf.org' **** //
		
		$ret = mail($to_email, 'Impact Story', implode("\r\n", $message), implode("\r\n", $headers));


		?>
		<div class="h1 text-center">Thank you for your submission</div>
		<?php if (!$ret) echo '<p class="text-center">oops</p>'; ?>
		
	<?php else:
		doForm($err_name, $err_email);
	endif;
}
else {
	// some default values //
	$_REQUEST['may-contact'] = 'yes';
	$_REQUEST['contact-me'] = 'yes';
	$_REQUEST['i-am-a'] = 'Alumni';
	$_REQUEST['i-authorize'] = 1;

	// $_REQUEST['name'] = 'test';
	// $_REQUEST['email'] = 'bubba@gump.cc';
	doForm(false, false);
}






function doForm($err_name, $err_email ) { ?>

<form method="post" action="your-gifts-impact.php">
	<div class="row">	
		<div class="col-sm-6">
			<div class="form-group <?php if ($err_name) echo 'has-error has-feedback'; ?>">
				<label for="name">Name <span class="req">*</span></label>
				<input type="text" class="form-control" id="name" placeholder="Name" name="name" required="required" value="<?php echo $_REQUEST['name'];?>" />
				
				<?php if ($err_name): ?>
					<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
  					<span class="sr-only">(error)</span>
  				<?php endif; ?>
			</div>
			<div class="form-group <?php if ($err_email) echo 'has-error has-feedback'; ?>">
				<label for="email">Email <span class="req">*</span></label>
				<input type="email" class="form-control" id="email" placeholder="Email" name="email" required="required" value="<?php echo $_REQUEST['email'];?>"/>

				<?php if ($err_email): ?>
					<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
  					<span class="sr-only">(error)</span>
  				<?php endif; ?>
			</div>
			<div class="form-group">
				<label for="i-am-a">I am A/An:</label>
				<select class="form-control" id="i-am-a" name="i-am-a">
					<option <?php if ($_REQUEST['i-am-a'] == 'Alumni') echo 'selected="selected"';?>>Alumni</option>
					<option <?php if ($_REQUEST['i-am-a'] == 'Student') echo 'selected="selected"';?>>Student</option>
					<option <?php if ($_REQUEST['i-am-a'] == 'Faculty') echo 'selected="selected"';?>>Faculty</option>
				</select>
			</div>
		</div>
		<div class="col-sm-6">
			<label for="your-story">Your story:</label>
			<textarea class="form-control" rows="8" id="your-story" name="the-story"><?php echo $_REQUEST['the-story'];?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<p class="help-block">May we contact you to talk more about your story?</p>
				<label class="radio-inline">
					<input type="radio" name="may-contact" value="yes" <?php if ($_REQUEST['may-contact'] == 'yes') echo 'checked="checked"';?>/> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="may-contact" value="no" <?php if ($_REQUEST['may-contact'] == 'no') echo 'checked="checked"';?>/> No
				</label>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<p class="help-block">Are you interested in being contacted about sharing your story via video or audio recording?</p>
				<label class="radio-inline">
					<input type="radio" name="contact-me" value="yes" <?php if ($_REQUEST['contact-me'] == 'yes') echo 'checked="checked"';?> /> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="contact-me" value="no" <?php if ($_REQUEST['contact-me'] == 'no') echo 'checked="checked"';?> /> No
				</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="checkbox-inline">
					<input type="checkbox" name="i-authorize" <?php if (isset($_REQUEST['i-authorize'])) echo 'checked="checked"';?>> I authorize the MSU Alumni Foundation to use my story information in its marketing materials.
				</label>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<button type="submit" class="btn">Submit my story</button>
			</div>
		</div>
	</div>
</form>

<?php
}
?>