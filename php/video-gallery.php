<?php

?>
<div class="video-gallery">
	<div class="row">
		<!-- FIRST ONE GOES HERE -->
		<div class="col-sm-8">
			<?php $classes = array('video', 'featured'); include($path . 'excerpts/excerpt.php'); ?>
		</div>

		<!-- NEXT THREE GO HERE -->
		<div class="col-sm-4">
			<div class="vertical-carousel">
				<div><a href="#" title="up" class=""><span class="glyphicon glyphicon-chevron-up"></span></a></div>

				<div class="window">
					<div class="traveler">
						<?php 

						$classes = array('video', 'col2'); 
						include($path . 'excerpts/excerpt.php'); 
						include($path . 'excerpts/excerpt.php'); 
						include($path . 'excerpts/excerpt.php'); 
						include($path . 'excerpts/excerpt.php'); 
						include($path . 'excerpts/excerpt.php'); 
						?>
					</div>
				</div>
				<div><a href="#" title="down" class=""><span class="glyphicon glyphicon-chevron-down"></span></a></div>
			</div>
		</div>
	</div>
</div>
