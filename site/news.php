<!doctype html>
<html lang="en-us">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>The Campaign for Montana State University</title>

		<script src="js/modernizr.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="author" content="Mills College" />
		<meta name="robots" content="all" />
	    	<meta name="apple-mobile-web-app-title" content="The Campaign for Montana State University" />
	    	<link rel="shortcut icon" href="http://www.montana.edu/favicon.ico"/>
		
		<link rel="stylesheet" href="css/styles.css" />

		<!-- page-specific stylesheets -->
	    	 

	    	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
		<script src="js/interactions.js"></script>

	    	<!-- page-specific javascripts -->
	    	 


		<!-- GOOGLE CUSTOM SEARCH -->
	    	<script type="text/javascript">
	    		
	    		window.__gcse = {
				parsetags: 'explicit',
				callback: function(){
					if (document.readyState == 'complete') {
						google.search.cse.element.go('searchbox');
						google.search.cse.element.go('search-results');
						$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
					}
					else {
						google.setOnLoadCallback(function(){
							google.search.cse.element.go('searchbox');
							google.search.cse.element.go('search-results');
							$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
						});
					}
				}
			};

			(function($) {
				var cx = '014965969058198357657:q5oh1uiq1ui';//'009057559413264856654:ykk_ospy3gc'; //; '003587693938171614370:inikzu6wtdw'
				var gcse = document.createElement('script'); 
				gcse.type = 'text/javascript'; 
				gcse.async = true;
				gcse.src = (document.location.protocol == 'https' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
				var s = document.getElementsByTagName('script')[0]; 
				s.parentNode.insertBefore(gcse, s);
			})(jQuery);
	        </script>
	</head>
	<body class="news archive">
		<header>
			<div id="top-bar" class="blue-bg">
				<div class="container-fluid">
					<div>
						<a href="http://www.montana.edu/" title="MONTANA STATE"><img src="img/mts-logo.svg" alt=""/></a>
						
						<div class="pull-right">
							<a href="#searchbox"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
								<span class="glyphicon-search glyphicon"></span>
								<span class="icon-cancel"></span>
								<span class="sr-only">menu</span>
							</a> 
						</div>
					</div>
					<div id="searchbox" class="collapse">
						<div><div class="gcse-searchbox-only" data-resultsUrl="search-results.html"></div></div>
					</div>
				</div>
			</div>
			<div id="logo-row">
				<div class="container-fluid">
					<div><a href="index.php" title="campaign home"><img src="img/main-logo.svg" alt=""/></a></div>
					<div class="pull-right">
						<a href="#main-nav"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
							<span class="">menu</span>
							<span class="icon-menu"></span>
							<span class="icon-cancel"></span>
						</a> 
					</div>
				</div>
			</div>
			
			<div class="container-fluid">
				<nav class="navbar navbar-default">
					<div class="collapse navbar-collapse" id="main-nav">
						<ul class="nav navbar-nav">
							<li><a href="areas-to-support.html" title="Areas to Support">Areas to Support</a></li>
							<li><a href="how-to-help.html" title="How to Help">How to Help</a></li>
							<li><a href="your-gifts-impact.php" title="Your Gift's Impact">Your Gift&rsquo;s Impact</a></li>
							<li class="active"><a href="news.php" title="News">News</a></li>
							<li><a href="contact.html" title="Contact">Contact</a></li>
							<li><a href="#" title="Give Now" class="btn">Give Now</a></li>
						</ul>						
					</div>
				</nav>
			</div>



		</header> 
		<main>
<section class="simple-intro">
	<h1 class="text-center">Campaign News</h1>
	<div class="container-fluid pagination-container" id="news">
		<div class="pagination-canvas">
			<?php

			// LOAD THE NEWS.XML DATA INTO AN ARRAY //
			$items = array();
			$xml = new SimpleXMLElement(file_get_contents('news/news_rss.xml'));

			foreach($xml->channel[0]->item as $child):
				$items[] = $child;
			endforeach;

			// ROWS OF 3 EACH //
			$rows = array_chunk($items, 3); 

			// PAGES ARE 2 ROWS EACH //
			$pages = array_chunk($rows, 2);

			// WHAT PAGE IS REQUESTED? //
			$page = (isset($_GET['page']) && $_GET['page']) ? intval($_GET['page']) : 1; 

			// TIMEZONE //
			date_default_timezone_set('America/Los_Angeles');

			// ITERATE AND ECHO THE PAGE OF ITEMS //
			foreach($pages[$page-1] as $row): ?>
				<div class="row">
					<?php foreach($row as $item): 
						$ns = $item->getNamespaces(true);
						$media = $item->children($ns['media']); ?>
						<div class="col-sm-4">
							<a href="<?php echo $item->link;?>" title="<?php echo $item->title;?>" class="excerpt news">
								<div class="thumb">
									<div class="wrap16x9">
										<img src="<?php echo $media->content->thumbnail->attributes()->url;?>" alt=""> <!-- SMALL THUMBNAIL -->
									</div>
								</div>
								<div class="content">
									<div class="date"><?php echo date('F d, Y', strtotime($item->pubDate));?></div>
									<div class="area">people</div>
									<div class="headline"><?php echo $item->description;?></div>
									<div class="cta">Read full article</div>
								</div>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endforeach; ?>

			<!-- PAGINATION -->
			<nav>
				<ul class="pagination">
					<li <?php if($page == 1) echo 'class="disabled"';?>>
						<a href="?page=<?php echo $page - 1;?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
					</li>
					
					<?php for($i = 0; $i < count($pages); $i++): ?>
						<li<?php if($i + 1 == $page) echo ' class="active"';?>>
							<a href="?page=<?php echo $i + 1;?>"><?php echo $i + 1;?></a>
						</li>
					<?php endfor; ?>
					
					<li <?php if($page >= count($pages)) echo 'class="disabled"';?>>
						<a href="?page=<?php echo $page + 1;?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
					</li>			
				</ul>
			</nav>
		</div>
	</div>
</section>

<section class="campaing-progress-section">
	<div class="container-fluid top-ruled">
		<!-- START Fundraiser -->  
<div class="campaign-progress clearfix archive">        
	<div class="h2 text-center">campaign progress</div>
	<div class="left-side">
		<div class="progress-canvas">
			<div class="indicator_curValue"></div>
			<div class="indicator_bar_bg"></div>
			<div class="indicator_counter counter-graphic-left">
				<span class="indicator_counter_val">$181,400,000</span>
				<span class="indicator_counter_val_max">$300,000,000</span>
				<span class="indicator_counter_percent"></span>
			</div>
		</div>
	</div>
	<div class="right-side">
		<div>Our Goal</div>
		<div>$300 Million</div>
	</div>
</div>
<!-- END Fundraiser -->	</div>
</section>

		</main>

		<footer class="blue-bg">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<a href="http://www.msuaf.org/s/1584/start.aspx" title="Alumni Foundation"><img src="img/foundation-logo.png" alt="" class="img-responsive"/></a>
					</div>
					<div class="col-sm-3">
						<div class="h5">contact us</div>
						<p class="sentinal-book-it">1501 South 11th Avenue<br/>Bozeman, MT 59715<br/>800-457-1696</p>
					</div>
					<div class="col-sm-3">
						<div class="h5">stay connected</div>
						<p class="social-links">
							<a href="http://www.facebook.com/mtstatefoundation" title="Facebook"><span class="sr-only">Facebook</span><span class="icon-facebook"></span></a>
							<a href="http://twitter.com/MTStateFound" title="Twitter"><span class="sr-only">Twitter</span><span class="icon-twitter"></span></a>
							<a href="http://www.vimeo.com" title="Vimeo"><span class="sr-only">Vimeo</span><span class="icon-vimeo"></span></a>
						</p>
					</div>
					<div class="col-sm-3">
						<a class="btn" title="MSU ALUMNI FOUNDATION HOME" href="http://www.msuaf.org/s/1584/start.aspx">MSU ALUMNI<br/>FOUNDATION HOME</a>
					</div>
				</div>
			</div>
		</footer>

		<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header"> <!-- BEN -->
						<a id="videoClose" data-dismiss="modal" aria-hidden="true" title="close">
							<span class="icon-cancel"></span>
							<span class="sr-only">Close</span>
						</a>
					</div> <!-- BEN -->
					<div class="modal-body">
						<div class="wrap16x9">
							<iframe width="640" height="480" src="#" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>