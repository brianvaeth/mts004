<!doctype html>
<html lang="en-us">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>The Campaign for Montana State University</title>

		<script src="js/modernizr.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="author" content="Mills College" />
		<meta name="robots" content="all" />
	    	<meta name="apple-mobile-web-app-title" content="The Campaign for Montana State University" />
	    	<link rel="shortcut icon" href="http://www.montana.edu/favicon.ico"/>
		
		<link rel="stylesheet" href="css/styles.css" />

		<!-- page-specific stylesheets -->
	    	 

	    	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
		<script src="js/interactions.js"></script>

	    	<!-- page-specific javascripts -->
	    	 


		<!-- GOOGLE CUSTOM SEARCH -->
	    	<script type="text/javascript">
	    		
	    		window.__gcse = {
				parsetags: 'explicit',
				callback: function(){
					if (document.readyState == 'complete') {
						google.search.cse.element.go('searchbox');
						google.search.cse.element.go('search-results');
						$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
					}
					else {
						google.setOnLoadCallback(function(){
							google.search.cse.element.go('searchbox');
							google.search.cse.element.go('search-results');
							$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
						});
					}
				}
			};

			(function($) {
				var cx = '014965969058198357657:q5oh1uiq1ui';//'009057559413264856654:ykk_ospy3gc'; //; '003587693938171614370:inikzu6wtdw'
				var gcse = document.createElement('script'); 
				gcse.type = 'text/javascript'; 
				gcse.async = true;
				gcse.src = (document.location.protocol == 'https' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
				var s = document.getElementsByTagName('script')[0]; 
				s.parentNode.insertBefore(gcse, s);
			})(jQuery);
	        </script>
	</head>
	<body class="home ">
		<header>
			<div id="top-bar" class="blue-bg">
				<div class="container-fluid">
					<div>
						<a href="http://www.montana.edu/" title="MONTANA STATE"><img src="img/mts-logo.svg" alt=""/></a>
						
						<div class="pull-right">
							<a href="#searchbox"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
								<span class="glyphicon-search glyphicon"></span>
								<span class="icon-cancel"></span>
								<span class="sr-only">menu</span>
							</a> 
						</div>
					</div>
					<div id="searchbox" class="collapse">
						<div><div class="gcse-searchbox-only" data-resultsUrl="search-results.html"></div></div>
					</div>
				</div>
			</div>
			<div id="logo-row">
				<div class="container-fluid">
					<div><a href="index.php" title="campaign home"><img src="img/main-logo.svg" alt=""/></a></div>
					<div class="pull-right">
						<a href="#main-nav"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
							<span class="">menu</span>
							<span class="icon-menu"></span>
							<span class="icon-cancel"></span>
						</a> 
					</div>
				</div>
			</div>
			
			<div class="container-fluid">
				<nav class="navbar navbar-default">
					<div class="collapse navbar-collapse" id="main-nav">
						<ul class="nav navbar-nav">
							<li><a href="areas-to-support.html" title="Areas to Support">Areas to Support</a></li>
							<li><a href="how-to-help.html" title="How to Help">How to Help</a></li>
							<li><a href="your-gifts-impact.php" title="Your Gift's Impact">Your Gift&rsquo;s Impact</a></li>
							<li><a href="news.php" title="News">News</a></li>
							<li><a href="contact.html" title="Contact">Contact</a></li>
							<li><a href="#" title="Give Now" class="btn">Give Now</a></li>
						</ul>						
					</div>
				</nav>
			</div>



		</header> 
		<main>

<section class="jumbo home">
	<div>
		<div>
			<div class="content-box">
				
									<div class="h1">To create smarter, healthier communities.</div>
					<p>It takes Montana State University. And it takes you. <a href="areas-to-support.html" title="Learn more about the campaign">Learn more about the campaign</a></p>
				
			</div>
		</div>
	</div>


	<div>
		<div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6">
						<div class="call-to-action">
									<div><img src="img/home-hero-areas.jpg" alt=""/></div>
									<div class="headline">Imagine a better<br/>Montana.</div>
									<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
									<div><a href="how-to-help.php" title="Learn how you can help">Learn how you can help</a></div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="call-to-action">
									<div><img src="img/home-hero-how.jpg" alt=""/></div>
									<div class="headline">We&rsquo;re making a<br/>difference.</div>
									<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
									<div><a href="your-gifts-impact.php" title="Learn how you can help">See how your support helps</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

		
</section>

<section>
	<div class="container-fluid text-center constrained">
		<div class="h2">It takes all of us.</div>
		<p>We all have a story of how Montana State has impacted our communities and our lives, and your story can inspire others to create a better Montana.</p>
		<a class="btn" href="your-gifts-impact.php" title="share your story">share your story</a>
	</div>
</section>

<section class="top-ruled">
	<div class="container-fluid">
		<div class="video-gallery">
	<div class="row">
		<!-- FIRST ONE GOES HERE -->
		<div class="col-sm-8">
			<a href="https://player.vimeo.com/video/116267743?portrait=0&byline=0&title=0" title="lorem ipsum" class="excerpt video featured">
	<div class="thumb">
		<div class="wrap16x9">
			<img src="http://lorempixel.com/540/304/people/4" alt=""/> <!-- *** LARGE THUMBNAIL *** -->
		</div>
	</div>
	<div class="content">
		<div class="date">June 22, 2015</div>
		<div class="area">people</div>
		<div class="headline">It takes Dani Morrison</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		<div class="cta">play video</div>
	</div>
</a>		</div>

		<!-- NEXT THREE GO HERE -->
		<div class="col-sm-4">
			<div class="vertical-carousel">
				<div><a href="#" title="up" class=""><span class="glyphicon glyphicon-chevron-up"></span></a></div>

				<div class="window">
					<div class="traveler">
						<a href="https://player.vimeo.com/video/116267743?portrait=0&byline=0&title=0" title="lorem ipsum" class="excerpt video col2">
	<div class="thumb">
		<div class="wrap16x9">
			<img src="http://lorempixel.com/350/197/people/4" alt=""/> <!-- SMALL THUMBNAIL -->
		</div>
	</div>
	<div class="content">
		<div class="date">June 22, 2015</div>
		<div class="area">people</div>
		<div class="headline">It takes Dani Morrison</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		<div class="cta">play video</div>
	</div>
</a><a href="https://player.vimeo.com/video/116267743?portrait=0&byline=0&title=0" title="lorem ipsum" class="excerpt video col2">
	<div class="thumb">
		<div class="wrap16x9">
			<img src="http://lorempixel.com/350/197/people/4" alt=""/> <!-- SMALL THUMBNAIL -->
		</div>
	</div>
	<div class="content">
		<div class="date">June 22, 2015</div>
		<div class="area">people</div>
		<div class="headline">It takes Dani Morrison</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		<div class="cta">play video</div>
	</div>
</a><a href="https://player.vimeo.com/video/116267743?portrait=0&byline=0&title=0" title="lorem ipsum" class="excerpt video col2">
	<div class="thumb">
		<div class="wrap16x9">
			<img src="http://lorempixel.com/350/197/people/4" alt=""/> <!-- SMALL THUMBNAIL -->
		</div>
	</div>
	<div class="content">
		<div class="date">June 22, 2015</div>
		<div class="area">people</div>
		<div class="headline">It takes Dani Morrison</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		<div class="cta">play video</div>
	</div>
</a><a href="https://player.vimeo.com/video/116267743?portrait=0&byline=0&title=0" title="lorem ipsum" class="excerpt video col2">
	<div class="thumb">
		<div class="wrap16x9">
			<img src="http://lorempixel.com/350/197/people/4" alt=""/> <!-- SMALL THUMBNAIL -->
		</div>
	</div>
	<div class="content">
		<div class="date">June 22, 2015</div>
		<div class="area">people</div>
		<div class="headline">It takes Dani Morrison</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		<div class="cta">play video</div>
	</div>
</a><a href="https://player.vimeo.com/video/116267743?portrait=0&byline=0&title=0" title="lorem ipsum" class="excerpt video col2">
	<div class="thumb">
		<div class="wrap16x9">
			<img src="http://lorempixel.com/350/197/people/4" alt=""/> <!-- SMALL THUMBNAIL -->
		</div>
	</div>
	<div class="content">
		<div class="date">June 22, 2015</div>
		<div class="area">people</div>
		<div class="headline">It takes Dani Morrison</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		<div class="cta">play video</div>
	</div>
</a>					</div>
				</div>
				<div><a href="#" title="down" class=""><span class="glyphicon glyphicon-chevron-down"></span></a></div>
			</div>
		</div>
	</div>
</div>
	</div>
</section>

<section class="top-ruled">
	<div class="container-fluid">
		<!-- START Fundraiser -->  
<div class="campaign-progress clearfix Array">        
	<div class="h2 text-center">campaign progress</div>
	<div class="left-side">
		<div class="progress-canvas">
			<div class="indicator_curValue"></div>
			<div class="indicator_bar_bg"></div>
			<div class="indicator_counter counter-graphic-left">
				<span class="indicator_counter_val">$181,400,000</span>
				<span class="indicator_counter_val_max">$300,000,000</span>
				<span class="indicator_counter_percent"></span>
			</div>
		</div>
	</div>
	<div class="right-side">
		<div>Our Goal</div>
		<div>$300 Million</div>
	</div>
</div>
<!-- END Fundraiser -->	</div>
</section>


<section class="jumbo areas">
	<div>
		<div>
			<div class="content-box">
				
									<div class="h1">Make a lasting impact.</div>
					<p>Designate a gift or pledge to an area of personal interest or give to the university&rsquo;s area of greatest need. By giving, you help create new opportunities for our students, enrich our faculty and staff, strengthen our academic programs and enhance our campus community.</p>
					<a href="areas-to-support.html" class="btn" title="Areas to support">Areas to support</a>
				
			</div>
		</div>
	</div>

		
</section>
<section>
	<div class="container-fluid">
		<h2 class="text-center">Recent News</h2>
		<h5 class="text-center"><a href="news.php" title="all news">All News</a></h5>
		<hr/>

		<?php

		// LOAD THE NEWS.XML DATA INTO AN ARRAY //
		$items = array();
		$xml = new SimpleXMLElement(file_get_contents('news/news_rss.xml'));

		foreach($xml->channel[0]->item as $child):
			$items[] = $child;
		endforeach;

		// ROWS OF 3 EACH //
		$rows = array_chunk($items, 3); 

		// TIMEZONE //
		date_default_timezone_set('America/Los_Angeles');

		// ITERATE AND ECHO THE FIRST ROW //  ?>
		<div class="row">
			<?php foreach($rows[0] as $item): 
				$ns = $item->getNamespaces(true);
				$media = $item->children($ns['media']); ?>
				<div class="col-sm-4">
					<a href="<?php echo $item->link;?>" title="<?php echo $item->title;?>" class="excerpt news">
						<div class="thumb">
							<div class="wrap16x9">
								<img src="<?php echo $media->content->thumbnail->attributes()->url;?>" alt=""> <!-- SMALL THUMBNAIL -->
							</div>
						</div>
						<div class="content">
							<div class="date"><?php echo date('F d, Y', strtotime($item->pubDate));?></div>
							<div class="area">people</div>
							<div class="headline"><?php echo $item->description;?></div>
							<div class="cta">Read full article</div>
						</div>
					</a>
				</div>
			<?php endforeach; ?>
		</div>

	</div>
</section>

		</main>

		<footer class="blue-bg">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<a href="http://www.msuaf.org/s/1584/start.aspx" title="Alumni Foundation"><img src="img/foundation-logo.png" alt="" class="img-responsive"/></a>
					</div>
					<div class="col-sm-3">
						<div class="h5">contact us</div>
						<p class="sentinal-book-it">1501 South 11th Avenue<br/>Bozeman, MT 59715<br/>800-457-1696</p>
					</div>
					<div class="col-sm-3">
						<div class="h5">stay connected</div>
						<p class="social-links">
							<a href="http://www.facebook.com/mtstatefoundation" title="Facebook"><span class="sr-only">Facebook</span><span class="icon-facebook"></span></a>
							<a href="http://twitter.com/MTStateFound" title="Twitter"><span class="sr-only">Twitter</span><span class="icon-twitter"></span></a>
							<a href="http://www.vimeo.com" title="Vimeo"><span class="sr-only">Vimeo</span><span class="icon-vimeo"></span></a>
						</p>
					</div>
					<div class="col-sm-3">
						<a class="btn" title="MSU ALUMNI FOUNDATION HOME" href="http://www.msuaf.org/s/1584/start.aspx">MSU ALUMNI<br/>FOUNDATION HOME</a>
					</div>
				</div>
			</div>
		</footer>

		<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header"> <!-- BEN -->
						<a id="videoClose" data-dismiss="modal" aria-hidden="true" title="close">
							<span class="icon-cancel"></span>
							<span class="sr-only">Close</span>
						</a>
					</div> <!-- BEN -->
					<div class="modal-body">
						<div class="wrap16x9">
							<iframe width="640" height="480" src="#" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>