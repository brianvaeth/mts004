<!doctype html>
<html lang="en-us">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>The Campaign for Montana State University</title>

		<script src="js/modernizr.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="author" content="Mills College" />
		<meta name="robots" content="all" />
	    	<meta name="apple-mobile-web-app-title" content="The Campaign for Montana State University" />
	    	<link rel="shortcut icon" href="http://www.montana.edu/favicon.ico"/>
		
		<link rel="stylesheet" href="css/styles.css" />

		<!-- page-specific stylesheets -->
	    	 

	    	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
		<script src="js/interactions.js"></script>

	    	<!-- page-specific javascripts -->
	    	 


		<!-- GOOGLE CUSTOM SEARCH -->
	    	<script type="text/javascript">
	    		
	    		window.__gcse = {
				parsetags: 'explicit',
				callback: function(){
					if (document.readyState == 'complete') {
						google.search.cse.element.go('searchbox');
						google.search.cse.element.go('search-results');
						$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
					}
					else {
						google.setOnLoadCallback(function(){
							google.search.cse.element.go('searchbox');
							google.search.cse.element.go('search-results');
							$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
						});
					}
				}
			};

			(function($) {
				var cx = '014965969058198357657:q5oh1uiq1ui';//'009057559413264856654:ykk_ospy3gc'; //; '003587693938171614370:inikzu6wtdw'
				var gcse = document.createElement('script'); 
				gcse.type = 'text/javascript'; 
				gcse.async = true;
				gcse.src = (document.location.protocol == 'https' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
				var s = document.getElementsByTagName('script')[0]; 
				s.parentNode.insertBefore(gcse, s);
			})(jQuery);
	        </script>
	</head>
	<body class="impact archive">
		<header>
			<div id="top-bar" class="blue-bg">
				<div class="container-fluid">
					<div>
						<a href="http://www.montana.edu/" title="MONTANA STATE"><img src="img/mts-logo.svg" alt=""/></a>
						
						<div class="pull-right">
							<a href="#searchbox"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
								<span class="glyphicon-search glyphicon"></span>
								<span class="icon-cancel"></span>
								<span class="sr-only">menu</span>
							</a> 
						</div>
					</div>
					<div id="searchbox" class="collapse">
						<div><div class="gcse-searchbox-only" data-resultsUrl="search-results.html"></div></div>
					</div>
				</div>
			</div>
			<div id="logo-row">
				<div class="container-fluid">
					<div><a href="index.php" title="campaign home"><img src="img/main-logo.svg" alt=""/></a></div>
					<div class="pull-right">
						<a href="#main-nav"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
							<span class="">menu</span>
							<span class="icon-menu"></span>
							<span class="icon-cancel"></span>
						</a> 
					</div>
				</div>
			</div>
			
			<div class="container-fluid">
				<nav class="navbar navbar-default">
					<div class="collapse navbar-collapse" id="main-nav">
						<ul class="nav navbar-nav">
							<li><a href="areas-to-support.html" title="Areas to Support">Areas to Support</a></li>
							<li><a href="how-to-help.html" title="How to Help">How to Help</a></li>
							<li class="active"><a href="your-gifts-impact.php" title="Your Gift's Impact">Your Gift&rsquo;s Impact</a></li>
							<li><a href="news.php" title="News">News</a></li>
							<li><a href="contact.html" title="Contact">Contact</a></li>
							<li><a href="#" title="Give Now" class="btn">Give Now</a></li>
						</ul>						
					</div>
				</nav>
			</div>



		</header> 
		<main>
<section class="simple-intro">
	<div class="container-fluid">
		<h1>Your gift&rsquo;s impact</h1>
	</div>
</section>

<section>
	<div class="container-fluid top-ruled">
		<h2 class="text-center">watch what it takes</h2>

		<div class="pagination-container" id="videos">
			<div class="pagination-canvas">
					<?php

				// LOAD THE NEWS.XML DATA INTO AN ARRAY //
				$items = array();
				$xml = new SimpleXMLElement(file_get_contents('videos/videos_rss.xml'));
				foreach($xml->channel[0]->item as $child):
					$items[] = $child;
				endforeach;

				// ROWS OF 3 EACH //
				$rows = array_chunk($items, 3); 

				// PAGES ARE 2 ROWS EACH //
				$pages = array_chunk($rows, 2);

				// WHAT PAGE IS REQUESTED? //
				$page = (isset($_GET['p']) && $_GET['p']) ? intval($_GET['p']) : 1; 

				// ITERATE AND ECHO THE PAGE OF ITEMS //
				foreach($pages[$page-1] as $row): ?>
					<div class="row">
						<?php foreach($row as $item): 
							$ns = $item->getNamespaces(true);
							$media = $item->children($ns['media']);
							$ouc = $item->children($ns['ouc']);?>
							<div class="col-sm-4">
								<a href="<?php echo $ouc->videourl;?>" title="<?php echo $item->title;?>" class="excerpt video">
									<div class="thumb">
										<div class="wrap16x9">
											<img src="<?php echo $media->content->thumbnail->attributes()->url;?>" alt=""> <!-- SMALL THUMBNAIL -->
										</div>
									</div>
									<div class="content">
										<div class="headline"><?php echo $item->title;?></div>
										<p><?php echo $item->description;?></p>
										<div class="cta">Play video</div>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endforeach; ?>

				<!-- PAGINATION -->
				<nav>
					<ul class="pagination">
						<li <?php if($page == 1) echo 'class="disabled"';?>>
							<a href="?p=<?php echo $page - 1;?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
						</li>
						
						<?php for($i = 0; $i < count($pages); $i++): ?>
							<li<?php if($i + 1 == $page) echo ' class="active"';?>>
								<a href="?p=<?php echo $i + 1;?>"><?php echo $i + 1;?></a>
							</li>
						<?php endfor; ?>
						
						<li <?php if($page >= count($pages)) echo 'class="disabled"';?>>
							<a href="?p=<?php echo $page + 1;?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
						</li>			
					</ul>
				</nav>
			</div>
		</div>		

	</div>
</section>

<section>
	<div class="container-fluid top-ruled">
		<h2 class="text-center">read what it takes</h2>

		<div class="pagination-container" id="stories">
			<div class="pagination-canvas">
					<?php

				// LOAD THE NEWS.XML DATA INTO AN ARRAY //
				$items = array();
				$xml = new SimpleXMLElement(file_get_contents('impact-stories/stories_rss.xml'));
				foreach($xml->channel[0]->item as $child):
					$items[] = $child;
				endforeach;

				// ROWS OF 4 EACH //
				$rows = array_chunk($items, 4); 

				// PAGES ARE 2 ROWS EACH //
				$pages = array_chunk($rows, 2);

				// WHAT PAGE IS REQUESTED? //
				$page = (isset($_GET['page']) && $_GET['page']) ? intval($_GET['page']) : 1; 

				// ITERATE AND ECHO THE PAGE OF ITEMS //
				foreach($pages[$page-1] as $row): ?>
					<div class="row">
						<?php foreach($row as $item): 
							$ns = $item->getNamespaces(true);
							$media = $item->children($ns['media']);
							$ouc = $item->children($ns['ouc']);?>
							<div class="col-sm-3">
								<a href="<?php echo $item->link;?>" title="<?php echo $item->title;?>" class="excerpt">
									<div class="thumb">
										<div class="wrap16x9">
											<img src="<?php echo $media->content->thumbnail->attributes()->url;?>" alt=""> <!-- SMALL THUMBNAIL -->
										</div>
									</div>
									<div class="content">
										<div class="headline"><?php echo $item->title;?></div>
										<p><?php echo $item->description;?></p>
										<div class="cta"><?php echo $ouc->label;?></div>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endforeach; ?>

				<!-- PAGINATION -->
				<nav>
					<ul class="pagination">
						<li <?php if($page == 1) echo 'class="disabled"';?>>
							<a href="?page=<?php echo $page - 1;?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
						</li>
						
						<?php for($i = 0; $i < count($pages); $i++): ?>
							<li<?php if($i + 1 == $page) echo ' class="active"';?>>
								<a href="?page=<?php echo $i + 1;?>"><?php echo $i + 1;?></a>
							</li>
						<?php endfor; ?>
						
						<li <?php if($page >= count($pages)) echo 'class="disabled"';?>>
							<a href="?page=<?php echo $page + 1;?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
						</li>			
					</ul>
				</nav>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container-fluid top-ruled">
		<h2 class="text-center">share your story</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<p class="text-center">We all have a story of how Montana State has impacted our communities and our lives, and your story can inspire others to create a better Montana.</p>
			</div>
		</div>
			
		<div id="form-holder"><?php 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$err_name = !$_POST['name'];
	$err_email = !$_POST['email'];

	if (!$err_name && !$err_email) :

		// send an email //
		$headers   = array();
		$headers[] = "MIME-Version: 1.0";
		$headers[] = "Content-type: text/plain; charset=iso-8859-1";
		$headers[] = "Reply-To: {$_POST['name']} <{$_POST['email']}>";
		$headers[] = "From: {$_POST['name']} <{$_POST['email']}>";
		$headers[] = "Subject: Impact Story Submitted";
		$headers[] = "X-Mailer: PHP/".phpversion();

		// The message
		$message = array();
		$message[] = 'FORM VALUES';
		$message[] = '===========================================================';
		$message[] = 'Name: ' . $_POST['name'];
		$message[] = 'Email: ' . $_POST['email'];
		$message[] = 'I AM A/AN: ' . $_POST['i-am-a'];
		$message[] = 'You may contact me: ' . $_POST['may-contact'];
		$message[] = 'I am interested in providing video/audio: ' . $_POST['contact-me'];
		
		$message[] = '=========== MY STORY ======================';
		$message[] = wordwrap($_POST['the-story'], 70, "\r\n");

		$message[] = '==============================';

		if (isset($_POST['i-authorize']))
			$message[] = 'I authorize you to use my story';
		else
			$message[] = 'I DO NOT authorize you to use my story';

		// Send
		$to_email = 'bvaeth@gmail.com'; // *** should be: 'alumni@msuaf.org' **** //
		
		$ret = mail($to_email, 'Impact Story', implode("\r\n", $message), implode("\r\n", $headers));


		?>
		<div class="h1 text-center">Thank you for your submission</div>
		<?php if (!$ret) echo '<p class="text-center">oops</p>'; ?>
		
	<?php else:
		doForm($err_name, $err_email);
	endif;
}
else {
	// some default values //
	$_REQUEST['may-contact'] = 'yes';
	$_REQUEST['contact-me'] = 'yes';
	$_REQUEST['i-am-a'] = 'Alumni';
	$_REQUEST['i-authorize'] = 1;

	// $_REQUEST['name'] = 'test';
	// $_REQUEST['email'] = 'bubba@gump.cc';
	doForm(false, false);
}






function doForm($err_name, $err_email ) { ?>

<form method="post" action="your-gifts-impact.php">
	<div class="row">	
		<div class="col-sm-6">
			<div class="form-group <?php if ($err_name) echo 'has-error has-feedback'; ?>">
				<label for="name">Name <span class="req">*</span></label>
				<input type="text" class="form-control" id="name" placeholder="Name" name="name" required="required" value="<?php echo $_REQUEST['name'];?>" />
				
				<?php if ($err_name): ?>
					<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
  					<span class="sr-only">(error)</span>
  				<?php endif; ?>
			</div>
			<div class="form-group <?php if ($err_email) echo 'has-error has-feedback'; ?>">
				<label for="email">Email <span class="req">*</span></label>
				<input type="email" class="form-control" id="email" placeholder="Email" name="email" required="required" value="<?php echo $_REQUEST['email'];?>"/>

				<?php if ($err_email): ?>
					<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
  					<span class="sr-only">(error)</span>
  				<?php endif; ?>
			</div>
			<div class="form-group">
				<label for="i-am-a">I am A/An:</label>
				<select class="form-control" id="i-am-a" name="i-am-a">
					<option <?php if ($_REQUEST['i-am-a'] == 'Alumni') echo 'selected="selected"';?>>Alumni</option>
					<option <?php if ($_REQUEST['i-am-a'] == 'Student') echo 'selected="selected"';?>>Student</option>
					<option <?php if ($_REQUEST['i-am-a'] == 'Faculty') echo 'selected="selected"';?>>Faculty</option>
				</select>
			</div>
		</div>
		<div class="col-sm-6">
			<label for="your-story">Your story:</label>
			<textarea class="form-control" rows="8" id="your-story" name="the-story"><?php echo $_REQUEST['the-story'];?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<p class="help-block">May we contact you to talk more about your story?</p>
				<label class="radio-inline">
					<input type="radio" name="may-contact" value="yes" <?php if ($_REQUEST['may-contact'] == 'yes') echo 'checked="checked"';?>/> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="may-contact" value="no" <?php if ($_REQUEST['may-contact'] == 'no') echo 'checked="checked"';?>/> No
				</label>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<p class="help-block">Are you interested in being contacted about sharing your story via video or audio recording?</p>
				<label class="radio-inline">
					<input type="radio" name="contact-me" value="yes" <?php if ($_REQUEST['contact-me'] == 'yes') echo 'checked="checked"';?> /> Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="contact-me" value="no" <?php if ($_REQUEST['contact-me'] == 'no') echo 'checked="checked"';?> /> No
				</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="checkbox-inline">
					<input type="checkbox" name="i-authorize" <?php if (isset($_REQUEST['i-authorize'])) echo 'checked="checked"';?>> I authorize the MSU Alumni Foundation to use my story information in its marketing materials.
				</label>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<button type="submit" class="btn">Submit my story</button>
			</div>
		</div>
	</div>
</form>

<?php
}
?></div>
	</div>

	<script>
	(function($){
		$(document).ready(function(){
			var canvas = $('#form-holder');
			var form = canvas.find('form');

			form.on('submit', function(e){
				e.preventDefault();
				canvas.css('height', canvas.height()).animate({opacity:0}, 'fast', function(){
					$.post(window.location.href, form.serialize(), function(data){
						canvas.empty().append($(data).find('#form-holder'));
						canvas.animate({opacity:1}, 'fast');
					});
				});
			});
		});
	})(jQuery);
	</script>
</section>



<section>
	<div class="container-fluid top-ruled">
		<!-- START Fundraiser -->  
<div class="campaign-progress clearfix Array">        
	<div class="h2 text-center">campaign progress</div>
	<div class="left-side">
		<div class="progress-canvas">
			<div class="indicator_curValue"></div>
			<div class="indicator_bar_bg"></div>
			<div class="indicator_counter counter-graphic-left">
				<span class="indicator_counter_val">$181,400,000</span>
				<span class="indicator_counter_val_max">$300,000,000</span>
				<span class="indicator_counter_percent"></span>
			</div>
		</div>
	</div>
	<div class="right-side">
		<div>Our Goal</div>
		<div>$300 Million</div>
	</div>
</div>
<!-- END Fundraiser -->	</div>
</section>


		</main>

		<footer class="blue-bg">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">
						<a href="http://www.msuaf.org/s/1584/start.aspx" title="Alumni Foundation"><img src="img/foundation-logo.png" alt="" class="img-responsive"/></a>
					</div>
					<div class="col-sm-3">
						<div class="h5">contact us</div>
						<p class="sentinal-book-it">1501 South 11th Avenue<br/>Bozeman, MT 59715<br/>800-457-1696</p>
					</div>
					<div class="col-sm-3">
						<div class="h5">stay connected</div>
						<p class="social-links">
							<a href="http://www.facebook.com/mtstatefoundation" title="Facebook"><span class="sr-only">Facebook</span><span class="icon-facebook"></span></a>
							<a href="http://twitter.com/MTStateFound" title="Twitter"><span class="sr-only">Twitter</span><span class="icon-twitter"></span></a>
							<a href="http://www.vimeo.com" title="Vimeo"><span class="sr-only">Vimeo</span><span class="icon-vimeo"></span></a>
						</p>
					</div>
					<div class="col-sm-3">
						<a class="btn" title="MSU ALUMNI FOUNDATION HOME" href="http://www.msuaf.org/s/1584/start.aspx">MSU ALUMNI<br/>FOUNDATION HOME</a>
					</div>
				</div>
			</div>
		</footer>

		<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header"> <!-- BEN -->
						<a id="videoClose" data-dismiss="modal" aria-hidden="true" title="close">
							<span class="icon-cancel"></span>
							<span class="sr-only">Close</span>
						</a>
					</div> <!-- BEN -->
					<div class="modal-body">
						<div class="wrap16x9">
							<iframe width="640" height="480" src="#" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>