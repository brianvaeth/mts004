/**
 * jQiery inview min
 */
!function(t){function e(){var e,i,n={height:a.innerHeight,width:a.innerWidth};return n.height||(e=r.compatMode,(e||!t.support.boxModel)&&(i="CSS1Compat"===e?f:r.body,n={height:i.clientHeight,width:i.clientWidth})),n}function i(){return{top:a.pageYOffset||f.scrollTop||r.body.scrollTop,left:a.pageXOffset||f.scrollLeft||r.body.scrollLeft}}function n(){var n,l=t(),r=0;if(t.each(d,function(t,e){var i=e.data.selector,n=e.$element;l=l.add(i?n.find(i):n)}),n=l.length)for(o=o||e(),h=h||i();n>r;r++)if(t.contains(f,l[r])){var a,c,p,s=t(l[r]),u={height:s.height(),width:s.width()},g=s.offset(),v=s.data("inview");if(!h||!o)return;g.top+u.height>h.top&&g.top<h.top+o.height&&g.left+u.width>h.left&&g.left<h.left+o.width?(a=h.left>g.left?"right":h.left+o.width<g.left+u.width?"left":"both",c=h.top>g.top?"bottom":h.top+o.height<g.top+u.height?"top":"both",p=a+"-"+c,v&&v===p||s.data("inview",p).trigger("inview",[!0,a,c])):v&&s.data("inview",!1).trigger("inview",[!1])}}var o,h,l,d={},r=document,a=window,f=r.documentElement,c=t.expando;t.event.special.inview={add:function(e){d[e.guid+"-"+this[c]]={data:e,$element:t(this)},l||t.isEmptyObject(d)||(l=setInterval(n,250))},remove:function(e){try{delete d[e.guid+"-"+this[c]]}catch(i){}t.isEmptyObject(d)&&(clearInterval(l),l=null)}},t(a).bind("scroll resize scrollstop",function(){o=h=null}),!f.addEventListener&&f.attachEvent&&f.attachEvent("onfocusin",function(){h=null})}(jQuery);


/*! jQuery Mobile v1.3.2 | Copyright 2010, 2013 jQuery Foundation, Inc. | jquery.org/license */
//(function(e,t,n){typeof define=="function"&&define.amd?define(["jquery"],function(r){return n(r,e,t),r.mobile}):n(e.jQuery,e,t)})(this,document,function(e,t,n,r){(function(e,t,n,r){function x(e){while(e&&typeof e.originalEvent!="undefined")e=e.originalEvent;return e}function T(t,n){var i=t.type,s,o,a,l,c,h,p,d,v;t=e.Event(t),t.type=n,s=t.originalEvent,o=e.event.props,i.search(/^(mouse|click)/)>-1&&(o=f);if(s)for(p=o.length,l;p;)l=o[--p],t[l]=s[l];i.search(/mouse(down|up)|click/)>-1&&!t.which&&(t.which=1);if(i.search(/^touch/)!==-1){a=x(s),i=a.touches,c=a.changedTouches,h=i&&i.length?i[0]:c&&c.length?c[0]:r;if(h)for(d=0,v=u.length;d<v;d++)l=u[d],t[l]=h[l]}return t}function N(t){var n={},r,s;while(t){r=e.data(t,i);for(s in r)r[s]&&(n[s]=n.hasVirtualBinding=!0);t=t.parentNode}return n}function C(t,n){var r;while(t){r=e.data(t,i);if(r&&(!n||r[n]))return t;t=t.parentNode}return null}function k(){g=!1}function L(){g=!0}function A(){E=0,v.length=0,m=!1,L()}function O(){k()}function M(){_(),c=setTimeout(function(){c=0,A()},e.vmouse.resetTimerDuration)}function _(){c&&(clearTimeout(c),c=0)}function D(t,n,r){var i;if(r&&r[t]||!r&&C(n.target,t))i=T(n,t),e(n.target).trigger(i);return i}function P(t){var n=e.data(t.target,s);if(!m&&(!E||E!==n)){var r=D("v"+t.type,t);r&&(r.isDefaultPrevented()&&t.preventDefault(),r.isPropagationStopped()&&t.stopPropagation(),r.isImmediatePropagationStopped()&&t.stopImmediatePropagation())}}function H(t){var n=x(t).touches,r,i;if(n&&n.length===1){r=t.target,i=N(r);if(i.hasVirtualBinding){E=w++,e.data(r,s,E),_(),O(),d=!1;var o=x(t).touches[0];h=o.pageX,p=o.pageY,D("vmouseover",t,i),D("vmousedown",t,i)}}}function B(e){if(g)return;d||D("vmousecancel",e,N(e.target)),d=!0,M()}function j(t){if(g)return;var n=x(t).touches[0],r=d,i=e.vmouse.moveDistanceThreshold,s=N(t.target);d=d||Math.abs(n.pageX-h)>i||Math.abs(n.pageY-p)>i,d&&!r&&D("vmousecancel",t,s),D("vmousemove",t,s),M()}function F(e){if(g)return;L();var t=N(e.target),n;D("vmouseup",e,t);if(!d){var r=D("vclick",e,t);r&&r.isDefaultPrevented()&&(n=x(e).changedTouches[0],v.push({touchID:E,x:n.clientX,y:n.clientY}),m=!0)}D("vmouseout",e,t),d=!1,M()}function I(t){var n=e.data(t,i),r;if(n)for(r in n)if(n[r])return!0;return!1}function q(){}function R(t){var n=t.substr(1);return{setup:function(r,s){I(this)||e.data(this,i,{});var o=e.data(this,i);o[t]=!0,l[t]=(l[t]||0)+1,l[t]===1&&b.bind(n,P),e(this).bind(n,q),y&&(l.touchstart=(l.touchstart||0)+1,l.touchstart===1&&b.bind("touchstart",H).bind("touchend",F).bind("touchmove",j).bind("scroll",B))},teardown:function(r,s){--l[t],l[t]||b.unbind(n,P),y&&(--l.touchstart,l.touchstart||b.unbind("touchstart",H).unbind("touchmove",j).unbind("touchend",F).unbind("scroll",B));var o=e(this),u=e.data(this,i);u&&(u[t]=!1),o.unbind(n,q),I(this)||o.removeData(i)}}}var i="virtualMouseBindings",s="virtualTouchID",o="vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "),u="clientX clientY pageX pageY screenX screenY".split(" "),a=e.event.mouseHooks?e.event.mouseHooks.props:[],f=e.event.props.concat(a),l={},c=0,h=0,p=0,d=!1,v=[],m=!1,g=!1,y="addEventListener"in n,b=e(n),w=1,E=0,S;e.vmouse={moveDistanceThreshold:10,clickDistanceThreshold:10,resetTimerDuration:1500};for(var U=0;U<o.length;U++)e.event.special[o[U]]=R(o[U]);y&&n.addEventListener("click",function(t){var n=v.length,r=t.target,i,o,u,a,f,l;if(n){i=t.clientX,o=t.clientY,S=e.vmouse.clickDistanceThreshold,u=r;while(u){for(a=0;a<n;a++){f=v[a],l=0;if(u===r&&Math.abs(f.x-i)<S&&Math.abs(f.y-o)<S||e.data(u,s)===f.touchID){t.preventDefault(),t.stopPropagation();return}}u=u.parentNode}}},!0)})(e,t,n),function(e){e.mobile={}}(e),function(e,t){var r={touch:"ontouchend"in n};e.mobile.support=e.mobile.support||{},e.extend(e.support,r),e.extend(e.mobile.support,r)}(e),function(e,t,r){function l(t,n,r){var i=r.type;r.type=n,e.event.dispatch.call(t,r),r.type=i}var i=e(n);e.each("touchstart touchmove touchend tap taphold swipe swipeleft swiperight scrollstart scrollstop".split(" "),function(t,n){e.fn[n]=function(e){return e?this.bind(n,e):this.trigger(n)},e.attrFn&&(e.attrFn[n]=!0)});var s=e.mobile.support.touch,o="touchmove scroll",u=s?"touchstart":"mousedown",a=s?"touchend":"mouseup",f=s?"touchmove":"mousemove";e.event.special.scrollstart={enabled:!0,setup:function(){function s(e,n){r=n,l(t,r?"scrollstart":"scrollstop",e)}var t=this,n=e(t),r,i;n.bind(o,function(t){if(!e.event.special.scrollstart.enabled)return;r||s(t,!0),clearTimeout(i),i=setTimeout(function(){s(t,!1)},50)})}},e.event.special.tap={tapholdThreshold:750,setup:function(){var t=this,n=e(t);n.bind("vmousedown",function(r){function a(){clearTimeout(u)}function f(){a(),n.unbind("vclick",c).unbind("vmouseup",a),i.unbind("vmousecancel",f)}function c(e){f(),s===e.target&&l(t,"tap",e)}if(r.which&&r.which!==1)return!1;var s=r.target,o=r.originalEvent,u;n.bind("vmouseup",a).bind("vclick",c),i.bind("vmousecancel",f),u=setTimeout(function(){l(t,"taphold",e.Event("taphold",{target:s}))},e.event.special.tap.tapholdThreshold)})}},e.event.special.swipe={scrollSupressionThreshold:30,durationThreshold:1e3,horizontalDistanceThreshold:30,verticalDistanceThreshold:75,start:function(t){var n=t.originalEvent.touches?t.originalEvent.touches[0]:t;return{time:(new Date).getTime(),coords:[n.pageX,n.pageY],origin:e(t.target)}},stop:function(e){var t=e.originalEvent.touches?e.originalEvent.touches[0]:e;return{time:(new Date).getTime(),coords:[t.pageX,t.pageY]}},handleSwipe:function(t,n){n.time-t.time<e.event.special.swipe.durationThreshold&&Math.abs(t.coords[0]-n.coords[0])>e.event.special.swipe.horizontalDistanceThreshold&&Math.abs(t.coords[1]-n.coords[1])<e.event.special.swipe.verticalDistanceThreshold&&t.origin.trigger("swipe").trigger(t.coords[0]>n.coords[0]?"swipeleft":"swiperight")},setup:function(){var t=this,n=e(t);n.bind(u,function(t){function o(t){if(!i)return;s=e.event.special.swipe.stop(t),Math.abs(i.coords[0]-s.coords[0])>e.event.special.swipe.scrollSupressionThreshold&&t.preventDefault()}var i=e.event.special.swipe.start(t),s;n.bind(f,o).one(a,function(){n.unbind(f,o),i&&s&&e.event.special.swipe.handleSwipe(i,s),i=s=r})})}},e.each({scrollstop:"scrollstart",taphold:"tap",swipeleft:"swipe",swiperight:"swipe"},function(t,n){e.event.special[t]={setup:function(){e(this).bind(n,e.noop)}}})}(e,this)});


/**
 * CAMPAIGN PROGRESS
 */
(function($){
   var CampaignProgress = function(element, options)
   {
       var elem = $(element);
       var obj = this;
	   elem["dObj"] = obj;
	   var parent = $( elem ).parent();
	   var maxValue;
	   var curValue;
	   var curPercent;
	   var curValuePercent;
	   var myName = "foo";
	   //var tData = options["tData"];
	   

       // Merge options with defaults
       var settings = $.extend({
           param: 'defaultValue'
       }, options || {});

       // Public method
       
	this.init = function()
       	{	   
		   //== Get the current and max values
		   elem["obj"] = this;
		   curValue = $(elem).find(".indicator_counter_val").text().replace(/\D/g, '');
		   maxValue = $(elem).find(".indicator_counter_val_max").text().replace(/\D/g, '');
		   //== display precent
		   
		   this.initView();
		   
		
		//== update view on window resize  
 		 $(window).resize(function(){
 		     obj.resizeIt();
 		   });
       };
	   
	   this.initView = function(){
		  //== convert current campaignProgress amount to percentage
		   curPercent = (curValue/maxValue)*100;
		   curValuePercent = eval('"' + curPercent +'%"');
		   var curValuePercentDisplay = parseFloat(curPercent).toFixed(0);
		   $(elem).find(".indicator_counter_percent").html(curValuePercentDisplay + "%");
		  
		//== set the indicator_counter initial position
		 var nTop = $(elem).find(".indicator_bar_bg").height() +10;
		 var nLeft = 0;
		 var cTMPixel = 0;
		 
		 //== account for width of indicator_counter in relation to bars
		 if (curPercent >= 75){
			 nLeft = $(elem).find(".indicator_bar_bg").position().left - ($(elem).find(".indicator_counter").width());
			 cTMPixel = ($(parent).width()*(curPercent/100) - ($(elem).find(".indicator_counter").width()));
			 $(elem).find(".indicator_counter").addClass("counter-graphic-right");
			 
		 }else if (curPercent <= 10 ){
			nLeft = $(elem).find(".indicator_bar_bg").position().left;
			cTMPixel = ($(parent).width()*(curPercent/100));
			$(elem).find(".indicator_counter").addClass("counter-graphic-left");
			 
		 }else{
			 nLeft = $(elem).find(".indicator_bar_bg").position().left - ($(elem).find(".indicator_counter").width());
			 cTMPixel = ($(parent).width()*(curPercent/100) - ($(elem).find(".indicator_counter").width()/2));
			 $(elem).find(".indicator_counter").addClass("counter-graphic-center");
			 
		 }
		//== apply x offset and initial y position
		 $(elem).find(".indicator_counter").css({top: nTop, left:nLeft});
		//==
		 var cTMPercent = cTMPixel/$(parent).width();
		 var cTMPerStr = eval('"' + (cTMPercent*100) +'%"');
		
		//== show indicator and animate all elements into position
		 $(elem).find(".indicator_counter").show();
		 $(elem).find(".indicator_bar_bg").animate({width: "100%"},1000);
		 $(elem).find(".indicator_curValue").animate({width: curValuePercent},750);
		 $(elem).find(".indicator_counter").animate({left: cTMPerStr},750);
		 
		 //== create divider lines every 10% and animate into view
		 for(var i=1; i<10; i++){ 
			var disInPercent = (10 * i);
			if (disInPercent < curPercent){
				$(elem).append( "<div class='vertical_line_lite' style="+ eval('"left:' + disInPercent +'%"')+"></div>" );
				$(".vertical_line_lite").delay(50*i).fadeIn(500);
			}else{
				$(elem).append( "<div class='vertical_line' style="+ eval('"left:' + disInPercent +'%"')+"></div>" );
				$(".vertical_line").delay(50*i).fadeIn(500);
			}
			
		 }	
		 
	   };
	   
	   /*
		   
	   */
	   this.resizeIt = function(){	   
		   this.updateIndicatorCounter();
	   };
	   
	   /*
	   Update the indicator location 
	   */
	   this.updateIndicatorCounter = function(){
  		var curPercent = (curValue/maxValue)*100;
    	
		 var cTMPixel = 0;
		 if (curPercent >= 75){
			 cTMPixel = ($(parent).width()*(curPercent/100) - ($(elem).find(".indicator_counter").width()));
		 }else if (curPercent <= 10 ){
			cTMPixel = ($(parent).width()*(curPercent/100));
		 }else{	 
			 cTMPixel = ($(parent).width()*(curPercent/100) - ($(elem).find(".indicator_counter").width()/2));
			 
		 }

    	
    	
    	
    	
    	var cTMPercent = cTMPixel/$(parent).width();
    	var cTMPerStr = eval('"' + (cTMPercent*100) +'%"');
		$(elem).find(".indicator_counter").css({left: cTMPerStr});
	   };
	   
	   
	   
   };

   $.fn.campaignProgress = function(options)
   {
       return this.each(function()
       {
           var element = $(this);
          
           // Return early if this element already has a plugin instance
           if (element.data('campaign-progress')) return;

           // pass options to plugin constructor
           var campaignProgress = new CampaignProgress(this, options);

           // Store plugin object in this element's data
           element.data('campaign-progress', campaignProgress);
       });
   };

   $(document).ready(function(){
   	var canvas = $('.campaign-progress');
   	var progressBar = canvas.find('.progress-canvas');

   	// make a campaign progress thing //
   	progressBar.campaignProgress();

   	// bind an inView event handler to kick off the INIT when the canvas scrolls into view //
   	canvas.bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
   		if (isInView) {
    			// element is now visible in the viewport
    			if (visiblePartY == 'top') {
      				// top part of element is visible
			} 
			else if (visiblePartY == 'bottom') {
      				// bottom part of element is visible
			} 
			else {
      				// whole part of element is visible
      				progressBar.data('campaign-progress').init();
      				canvas.unbind('inview');
			}
		} 
		else {
    				// element has gone out of viewport
		}
	});
   });
})(jQuery);



/**
 * vertical j stuff
 */
(function($){
	$(document).ready(function(){
		$('.vertical-carousel').each(function(i, el){
			var canvas = $(el);
			var carousel = canvas.find('.window');
			var traveler = carousel.find('.traveler');
			var items = traveler.find('a.video');

			var prev = canvas.find('> div:first-child > a').attr('disabled', 'disabled');
			var next = canvas.find('> div:last-child > a');

			var thumb = canvas.parents('.video-gallery').find('.featured .thumb');

			// measure // 
			$(window).resize(function(){
				var height = thumb.height() - 60;
				var padding = Math.ceil((height - (items.height() * 3)) / 3);
				if (padding < 0) return;

				carousel.css('height', height);
				items.css('padding-bottom', Math.ceil(padding/2)).css('padding-top', Math.ceil(padding/2));
			}).resize();
			
			next.click(function(e){
				e.preventDefault();
				var height = carousel.height();
				var newScrollTop = height + carousel.scrollTop();
				
				carousel.animate({scrollTop: newScrollTop}, 'fast', function(){
					prev.attr('disabled', null);
					if ((newScrollTop + height + 30) >= traveler.height()) next.attr('disabled', 'disabled'); 
				});
			});

			prev.click(function(e){
				e.preventDefault();
				var height = carousel.height();
				var newScrollTop = carousel.scrollTop() - height;
				carousel.animate({scrollTop: newScrollTop}, 'fast', function(){
					next.attr('disabled', null);
					if ((newScrollTop - 30) <= 0) prev.attr('disabled', 'disabled'); 
				});
			});

		});
	});
})(jQuery);



/**
 * Add SWIPE TO BOOTSTRAP CAROUSEL
 */
// (function($){
// 	$(document).ready(function(){
// 		$('.carousel').swiperight(function() {  
// 			$(this).carousel('prev');  
// 		}).swipeleft(function() {  
// 			$(this).carousel('next');  
// 		});
// 	});
// })(jQuery);


/**
 * Adds the boostrap modal behavior to video excerpts
 */
(function($){
	$(document).ready(function(){
		var modal = $('#videoModal');

		$('main').on('click', 'a.excerpt.video', function(e){
			e.preventDefault();
			var url = $(this).attr('href');
			modal.find('iframe').attr('src', url);
			modal.modal('show');
		});

		modal.on('hidden.bs.modal', function(){
			modal.find('iframe').attr('src', 'about:blank');
		});
	});
})(jQuery);



/* AJAX ENABLE PAGINATION FOR NEWS AND STORIED */
(function($){
	$(document).ready(function(){
		$('.pagination-container').each(function(i, el){
			var canvas = $(el);
			var id = $(el).attr('id');

			canvas.on('click', '.pagination a', function(e){
				e.preventDefault();
				canvas.css('height', canvas.height()).animate({opacity:0}, 'fast', function(){					
					var thing = e.target;
					if (thing.nodeName == 'SPAN') thing = thing.parentElement;
					var href = $(thing).attr('href');
				
					$.get(href, function(data){
						canvas.empty().append($(data).find('#' + id)); 
						canvas.animate({opacity:1}, 'fast', function(){
							canvas.css('height', 'auto');
						});
					});
				});
			});
		});	
	});
})(jQuery);
