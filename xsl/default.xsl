<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE stylesheet [
<!ENTITY nbsp  "&#160;">
<!ENTITY rsquo  "&#8217;">
<!ENTITY amp  "&#38;">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ou="http://omniupdate.com/XSL/Variables" xmlns:fn="http://omniupdate.com/XSL/Functions" xmlns:ouc="http://omniupdate.com/XSL/Variables" exclude-result-prefixes="ou xsl xs fn ouc">
	
	<xsl:import href="_shared/template-matches.xsl"/>
	<xsl:import href="_shared/ou-variables.xsl"/>
	<xsl:import href="_shared/functions.xsl"/>


	<!-- Default: for HTML5 use below output declaration -->
	<xsl:output method="html" version="5.0" indent="yes" encoding="UTF-8" include-content-type="no"/>
	<xsl:strip-space elements="*"/>


	<xsl:variable name="months">
		<month>January</month>
		<month>February</month>
		<month>March</month>
		<month>April</month>
		<month>May</month>
		<month>June</month>
		<month>July</month>
		<month>August</month>
		<month>September</month>
		<month>October</month>
		<month>November</month>
		<month>December</month>
	</xsl:variable>

	<xsl:template match="/document">
		<html lang="en-us">
			<head>
				<meta charset="utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<title>The Campaign for Montana State University</title>

				<script src="/_resources/mts/js/modernizr.js"></script>
				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
				<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
				<!--[if lt IE 9]>
					<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
					<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
				<![endif]-->

				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<meta name="author" content="Mills College" />
				<meta name="robots" content="all" />
			    	<meta name="apple-mobile-web-app-title" content="The Campaign for Montana State University" />
			    	<link rel="shortcut icon" href="http://www.montana.edu/favicon.ico"/>
				
				<link rel="stylesheet" href="/_resources/mts/css/styles.css" />

				<!-- page-specific stylesheets -->
			    	 

			    	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
			    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
				<script src="/_resources/mts/js/interactions.js"></script>

			    	<!-- page-specific javascripts -->
			    	 

				<!-- GOOGLE CUSTOM SEARCH -->
			    	<script type="text/javascript">
			    		
			    		window.__gcse = {
						parsetags: 'explicit',
						callback: function(){
							if (document.readyState == 'complete') {
								google.search.cse.element.go('searchbox');
								google.search.cse.element.go('search-results');
								$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
							}
							else {
								google.setOnLoadCallback(function(){
									google.search.cse.element.go('searchbox');
									google.search.cse.element.go('search-results');
									$('#searchbox input.gsc-input').attr('placeholder', 'SEARCH');
								});
							}
						}
					};

					(function($) {
						var cx = '009057559413264856654:f4yjhrhfa8a';//'009057559413264856654:ykk_ospy3gc'; //; '003587693938171614370:inikzu6wtdw'
						var gcse = document.createElement('script'); 
						gcse.type = 'text/javascript'; 
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0]; 
						s.parentNode.insertBefore(gcse, s);
					})(jQuery);
			        </script>

			        <!-- Best practice is to have a couple common empty nodes in case a page needs to have page-unique code. These are only editable in source and therefore admins. -->
				<xsl:apply-templates select="headcode/node()" />
			</head>
			<body class="news single">
				
				<!-- Best practice is to have a couple common empty nodes in case a page needs to have page-unique code. These are only editable in source and therefore admins. -->
				<xsl:apply-templates select="bodycode/node()" />

				<header>
					<div id="top-bar" class="blue-bg">
						<div class="container-fluid">
							<div>
								<a href="http://www.montana.edu/" title="MONTANA STATE"><img src="/_resources/mts/img/mts-logo.svg" alt=""/></a>
								
								<div class="pull-right">
									<a href="#searchbox"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
										<span class="glyphicon-search glyphicon"></span>
										<span class="icon-cancel"></span>
										<span class="sr-only">menu</span>
									</a> 
								</div>
							</div>
							<div id="searchbox" class="collapse">
								<div><div class="gcse-searchbox-only" data-resultsUrl="/_resources/mts/search-results.html"></div></div>
							</div>
						</div>
					</div>
					<div id="logo-row">
						<div class="container-fluid">
							<div><a href="../index.php" title="campaign home"><img src="/_resources/mts/img/main-logo.svg" alt=""/></a></div>
							<div class="pull-right">
								<a href="#main-nav"  data-toggle="collapse" aria-expanded="false" class="collapsed hidden-sm hidden-md hidden-lg">
									<span class="">menu</span>
									<span class="icon-menu"></span>
									<span class="icon-cancel"></span>
								</a> 
							</div>
						</div>
					</div>
					
					<div class="container-fluid">
						<nav class="navbar navbar-default">
							<div class="collapse navbar-collapse" id="main-nav">
								<ul class="nav navbar-nav">
									<li><a href="../areas-to-support.html" title="Areas to Support">Areas to Support</a></li>
									<li><a href="../how-to-help.html" title="How to Help">How to Help</a></li>
									<li><a href="../your-gifts-impact.php" title="Your Gift's Impact">Your Gift&rsquo;s Impact</a></li>
									<li class="active"><a href="../news.php" title="News">News</a></li>
									<li><a href="../contact.html" title="Contact">Contact</a></li>
									<li><a href="#" title="Give Now" class="btn">Give Now</a></li>
								</ul>						
							</div>
						</nav>
					</div>



				</header> 
				<main>
					<article>
						<section id="breadcrumb">
							<div class="container-fluid">
								<ol class="breadcrumb">
									<li><a href="../index.php" title="home">Home</a></li>
									<li><a href="../news.php" title="news">News</a></li>
									<li class="active">Jabs Hall</li>
								</ol>
							</div>
						</section>

						<section class="sharethis">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
										<span class="st_facebook_large" displayText="Facebook"></span>
										<span class="st_twitter_large" displayText="Tweet"></span>
														</div>
								</div>
								<script src="http://w.sharethis.com/button/buttons.js"></script>
								<script>stLight.options({publisher: "88951ca8-4c02-405a-8bb0-a9d950c0c635", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
							</div>
						</section>

						<section class="headline">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
										<div class="dateline"><xsl:value-of select="$months/month[month-from-dateTime($ou:created)]"/>&nbsp;<xsl:value-of select="day-from-dateTime($ou:created)"/>, <xsl:value-of select="year-from-dateTime($ou:created)"/></div>
										<h1>Jabs Hall: Open for business</h1>
										<h2>Hundreds gather at MSU to celebrate the opening of Jabs Hall</h2>
									</div>
								</div>
							</div>
						</section>

						<section class="featured-image">
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
										<figure>
											<div class="wrap16x9"><img src="http://lorempixel.com/710/306/business/1" alt=""/></div>
											<figcaption>Lorem ipsum dolor sit amet, justo pellentesque a lectus sit, aliquam ut eget, et id. Lorem ipsum dolor sit amet, justo pellentesque a lectus sit, aliquam ut eget, et id.</figcaption>
										</figure>
									</div>
								</div>
							</div>
						</section>

						<section class="article-content">
							<div class="container-fluid constrained">


								<xsl:copy-of select="./*" />



							</div>
						</section>

					</article>
				</main>

				<footer class="blue-bg">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-3">
								<a href="http://www.msuaf.org/s/1584/start.aspx" title="Alumni Foundation"><img src="../img/foundation-logo.png" alt="" class="img-responsive"/></a>
							</div>
							<div class="col-sm-3">
								<div class="h5">contact us</div>
								<p class="sentinal-book-it">1501 South 11th Avenue<br/>Bozeman, MT 59715<br/>800-457-1696</p>
							</div>
							<div class="col-sm-3">
								<div class="h5">stay connected</div>
								<p class="social-links">
									<a href="#" title="Facebook"><span class="sr-only">Facebook</span><span class="icon-facebook"></span></a>
									<a href="#" title="Twitter"><span class="sr-only">Twitter</span><span class="icon-twitter"></span></a>
									<a href="#" title="Vimeo"><span class="sr-only">Vimeo</span><span class="icon-vimeo"></span></a>
								</p>
							</div>
							<div class="col-sm-3">
								<a class="btn" title="MSU ALUMNI FOUNDATION HOME" href="http://www.msuaf.org/s/1584/start.aspx">MSU ALUMNI<br/>FOUNDATION HOME</a>
							</div>
						</div>
					</div>
				</footer>

				<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header"> <!-- BEN -->
								<a id="videoClose" data-dismiss="modal" aria-hidden="true" title="close">
									<span class="icon-cancel"></span>
									<span class="sr-only">Close</span>
								</a>
							</div> <!-- BEN -->
							<div class="modal-body">
								<div class="wrap16x9">
									<iframe width="640" height="480" src="#" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Best practice is to have a couple common empty nodes in case a page needs to have page-unique code. These are only editable in source and therefore admins. -->
				<xsl:apply-templates select="footcode/node()" />

			</body>
		</html>
	</xsl:template>
	
	<!-- It is a best practice to define any templates that are being called in another XSL. This is to safe guard against any unwanted errors concerning calling templates. -->
	<xsl:template name="template-content" />
	<xsl:template name="additional-headcode" />
	<xsl:template name="additional-footcode" />
</xsl:stylesheet>